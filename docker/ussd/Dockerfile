FROM python:3.6-slim-stretch

RUN apt update \
  && apt -y install gcc g++ libffi-dev libstdc++-6-dev python3-dev musl-dev libssl-dev curl git

# GE MIGRATION
# For local OSX try:
# https://stackoverflow.com/questions/49025594/osx-ld-library-not-found-for-lssl
RUN apt -y install mysql-server default-libmysqlclient-dev

WORKDIR /app

COPY ./docker/ussd/entrypoint.sh .

COPY ./contrib/ussd/app/requirements.txt .
COPY ./contrib/ussd/test/requirements.txt test_requirements.txt

RUN pip install -r requirements.txt && pip install -r test_requirements.txt

RUN apt remove --purge -y gcc g++ libffi-dev libstdc++-6-dev python3-dev musl-dev libssl-dev

COPY ./contrib/ussd/config.py ./src/
COPY ./contrib/ussd/init.py ./src/

COPY ./contrib/ussd/app/server/ ./src/server
COPY ./contrib/ussd/app/migrations/ ./src/migrations
COPY ./contrib/ussd/files/ ./src/files
COPY ./contrib/ussd/app/manage.py ./src
COPY ./contrib/ussd/app/RedisQueue.py ./src
COPY ./contrib/ussd/test/ ./src/test
COPY ./contrib/ussd/invoke_tests.py ./src
COPY ./contrib/ussd/.coveragerc ./src
COPY ./contrib/ussd/share/ ./src/share

COPY ./src/common/config_files/* ./src/

WORKDIR /

RUN chmod +x /app/entrypoint.sh

ARG GIT_HASH
ENV GIT_HASH=$GIT_HASH

EXPOSE 9000

CMD ["/app/entrypoint.sh"]
