d=`cat <<EOF
{
	"email": "admin@acme.org",
	"password": "C0rrectH0rse"
}
EOF
`
#r=`curl -s -X POST "http://localhost:9000/api/v1/auth/request_api_token/" -d "$d" -H "Content-Type: application/json"`
echo 'curl -s -X POST "${APP_LOCATION}/api/v1/auth/request_api_token/" -d "$d" -H "Content-Type: application/json"'
r=`curl -s -X POST "${APP_LOCATION}/api/v1/auth/request_api_token/" -d "$d" -H "Content-Type: application/json --trace-ascii"`
echo "token $r"
token=`echo $r | jq -r '.auth_token' | sed -e 's/"//g'`
registry_address=`jq -r ".registry.address" tokens.json`
reserve_address=`jq -r ".reserve.address" tokens.json`
echo tokens:
jq -r '.tokens[]' tokens.json


i=0
for tt in `jq -r '.tokens[]' tokens.json`; do
	t=`jq -r ".tokens[$i]" tokens.json`
	#echo `jq -r ".tokens[${i}].symbol" tokens.json`
	t_address=`echo $t | jq -r '.address'`
	t_symbol=`echo $t | jq -r '.symbol'`
	t_name=`echo $t | jq -r '.name'`
	t_decimals=`echo $t | jq -r '.decimals'`
	t_converter=`echo $t | jq -r '.converter'`
	t_deployer=`echo $t | jq -r '.deployer'`
	t_weight=`echo $t | jq -r '.weight'`
	d=`cat <<EOF
{
	"name": "$t_name",
	"symbol": "$t_symbol",
	"decimals": "$t_decimals",
	"address": "$t_address",
	"is_reserve": false
}
EOF
`
	echo ">>>>>> token add request:"
	echo "$d"
	echo 
	r=`curl -X POST "${APP_LOCATION}/api/v1/token/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
	echo ">>>>>> token add response:"
	echo "$r"
	echo
	t_id=$(echo $r | jq '.data.token.id')

	d=`cat <<EOF
{
	"organisation_name": "$t_name Inc.",
	"custom_message_welcome_key": "${t_name}_org",
	"timezone": "Africa/Nairobi",
	"country_code": "KE",
	"account_address": "$t_deployer",
	"token_id": $t_id,
	"default_disbursement": 5000
}
EOF
`
	echo ">>>>>> org add request"
       	echo "$d"
	echo
	r=`curl -X POST "${APP_LOCATION}/api/v1/organisation/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
	echo ">>>>>> org add response"
	echo "$r"
	echo
	organisation_id=`echo $r | jq -r '.data.organisation.id'`



	d=`cat <<EOF
{
	"reserve_address": "$reserve_address",
        "token_address": "$t_address",
        "exchange_address": "$t_converter",
        "registry_address": "$registry_address",
        "reserve_ratio_ppm": "$t_weight",
	"organisation_id": $organisation_id
}
EOF
` 

	echo ">>>>>> converter add request"
	echo "$d"
	echo
	r=`curl -s -X POST "${APP_LOCATION}/api/v2/exchange/register/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
	echo ">>>>>> converter add response"
	echo "$r"
	echo


	d=`cat <<EOF
{
	"organisation_id": $organisation_id,
	"address": "$t_converter"
}
EOF
`
	r=`curl -X POST "${APP_LOCATION}/api/v2/transfer_account/register/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
	echo "transfer account results $r"

	d=`cat <<EOF
{
	"user_ids": [1],
	"is_admin": true
}
EOF
`
	r=`curl -X PUT "${APP_LOCATION}/api/v1/organisation/${organisation_id}/users/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`
	echo "add user 1 to org results $r"

	d=`cat <<EOF
{
	"email": "ge_${t_symbol}@sechost.info",
	"tier": "admin",
	"organisation_id": $organisation_id
}
EOF
`
	r=`curl -X POST "${APP_LOCATION}/api/v1/auth/permissions/" -d "$d" -H "Content-Type: application/json" -H "Authorization: $token"`

	echo $r
	i=$(($i+1))
	if [ "$i" -eq "2" ]; then
		break
	fi
done


