#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# GPG: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
# Description: Create a 100% clean deployment of the sarafu platform for use in development

#TRUFFLE_NETWORK_ID=$1
#BANCOR_NETWORK=42
#BANCOR_COMMIT=7b97453f1663ec79664df11119b888c3a5ad4823
#TRUFFLE_NETWORK_ID=development
#BANCOR_NETWORK=8995
BANCOR_NETWORK=${BANCOR_NETWORK:-42}
#BANCOR_COMMIT=421abfddc6eda78647deb7787e7246ac06fadb6b
BANCOR_COMMIT=${BANCOR_COMMIT:-d6e39d5fbdb95068e92379a9139f48021da2f95a}
wd=`pwd`

if [ -z $TRUFFLE_NETWORK_ID ]; then
	>&2 echo "usage $0 <network_key_from_truffle.config.js>"
fi

logfile=`mktemp`
>&2 echo logfile $logfile

d=$(dirname $(realpath $(dirname $0)))
#d_lib=${d}/var/lib
#mkdir -vp $d_lib
#ganache_dir="${d_lib}/ganache"
ganache_dir="$SEMPO_PATH/ganacheDB"
#if [ ! -f ${ganache_dir}/accounts ]; then
#	>&3 echo ganache accounts missing from ${ganache_dir}
#	3>&-
#	exit 1
#fi

# enter bancor dir and check for sanity
bancor_dir=$(realpath ${BANCOR_PATH:-"${d}/contrib/bancor"})
>&2 echo "bancordir $bancor_dir"
if [ -z $bancor_dir ]; then
	bancor_dir=$d/contrib/bancor_0.6
fi
if [ ! -d $bancor_dir ]; then
	>&2 echo "bancor dir '$bancor_dir' not a dir"
	exit 1
fi
pushd $bancor_dir
bancor_commit=`git rev-parse HEAD`
if [ "$bancor_commit" != "$BANCOR_COMMIT" ]; then
	>&2 echo "wrong bancor version, need $BANCOR_COMMIT, have $bancor_commit"
	exit 1
fi
if [ ! -d "node_modules" ]; then # risky, doesn't check the contents
	# snyk never completes..
	mv package.json package.json.tmp
	jq '.scripts.prepare = "echo skipping prepare snyk because it just neeeever finishes :/"' package.json.tmp | jq '.snyk = false' > package.json
	rm -f package-lock.json
	npm install
	npm install --save @truffle/hdwallet-provider
	mv package.json.tmp package.json
fi
if [ ! -f ${bancor_dir}/node_modules/truffle/build/cli.bundled.js ]; then
	>&2 echo "cannot find truffle bin"
	exit 1
fi
truffle=${bancor_dir}/node_modules/truffle/build/cli.bundled.js
pushd solidity

cp -v ${SRC_PATH}/bancor/solidity/truffle-config.js .
cp -Rv ${SRC_PATH}/bancor/solidity/migrations .

# build and deploy bancor contracts
#$truffle --network $TRUFFLE_NETWORK_ID migrate --reset --skip-dry-run
rm -vrf build
# call the compile separately because if not we may get ganache timeouts if there's a lag
$truffle --network $TRUFFLE_NETWORK_ID compile
$truffle --network $TRUFFLE_NETWORK_ID migrate --skip-dry-run --describe-json --verbose-rpc
if [ "$?" -gt 0 ]; then
	>&2 echo "truffle migrate fail"
	exit 1
fi
popd
popd
mv -v ${bancor_dir}/solidity/tokens.json .





# convenience variables for contracts
# needed by api calls for bootstrapping data
#contract_names=()
#contract_addresses=()
#token_address=''
#reserve_address=''
#registry_address=''
#i=0
#for f in `ls ${bancor_dir}/solidity/build/contracts/*.json`; do
#	# illegibly get the filename without extension
#	>&3 echo checking contract build json $f
#	k=${f%%.*}
#	k=${k##*/}
#	jleaf=".networks[\"${BANCOR_NETWORK}\"].address"
#	j=$(jq -r $jleaf $f)
#	# TODO: is null a valid option here?
#	if [ "$j" != 'null' ]; then
#		address=`python ${d}/tools/ethereum_checksum_address.py ${j}`
#		# TODO: likewise here, seems an invalid address should fail the script
#		if [ "$address" == 'null' ]; then
#			>&3 echo null address in $f
#			continue
#		fi
#		>&3 echo $k $address
#		contract_names[i]=$k
#		contract_addresses[i]=$address
#		if [ "$k" == "SmartToken" ]; then
#			token_address=$address
#		fi
#		if [ "$k" == "EtherToken" ]; then
#			reserve_address=$address
#		fi
#		if [ "$k" == "ContractRegistry" ]; then
#			registry_address=$address
#		fi
#		i=$((i+1))
#	fi
#done
#>&3 echo "bancor contract names ${contract_names[@]}"
#>&3 echo "bancor contract addresses ${contract_addresses[@]}"
#converter_address=`python ${d}/tools/bancor_converter_registry.py --endpoint http://localhost:8545 --bancor-dir ${bancor_dir} --registry-address $registry_address $token_address`


# determine master eth key

pks=($(cat keys.json | jq -r '.private[]'))
addrs=($(cat keys.json | jq -r '.address[]'))
#d="."
#master_private_key=`jq -r '.private_keys[]' ${ganache_dir}/keys.json | head -n 1`
master_private_key=${pks[0]}
#master_address=`python ${d}/tools/ethereum_checksum_address.py $(jq -r '.private_keys | to_entries[0].key' ${ganache_dir}/keys.json)`
master_address=${addrs[0]}
>&2 echo private key is $master_private_key
