with import <nixpkgs> { };
stdenv.mkDerivation {
  name = "pipeline-shell";
  buildInputs = [ (import ./default.nix {}).env ];
}
