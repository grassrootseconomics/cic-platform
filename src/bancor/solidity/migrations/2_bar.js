const fs = require('fs');
const bn = require('bn.js');
const Web3 = require('web3');

const LiquidTokenConverterFactory = artifacts.require('LiquidTokenConverterFactory');
const LiquidTokenConverter = artifacts.require('LiquidTokenConverter');
const ConverterRegistry = artifacts.require('ConverterRegistry');
const ContractRegistry = artifacts.require('ContractRegistry');
const BancorConverterFactory = artifacts.require('ConverterFactory');
const EtherToken = artifacts.require('EtherToken');
const ERC20Token = artifacts.require('ERC20Token');
const SmartToken = artifacts.require('SmartToken');
const BancorNetwork = artifacts.require('BancorNetwork');

const amount = new bn(process.env['ETH_DEV_INITIAL_BALANCE'], 10);
amount_bert_reserve_factor_percentage = new bn(10);
amount_ernie_reserve_factor_percentage = new bn(20);

let token_output = {}

//  we need to override because there is a bug showing wrong accounts for our provider
//web3 = new Web3('http://localhost:8545');

module.exports = function(deployer, network, accounts) {
	deployer.then(async() => {
		const converterFactory = await BancorConverterFactory.deployed();
		const converterRegistry = await ConverterRegistry.deployed();
		const etherToken = await EtherToken.deployed();
		const bancorNetwork = await BancorNetwork.deployed();
		const contractRegistry = await ContractRegistry.deployed();

		const liquidFactory = await deployer.deploy(LiquidTokenConverterFactory);
		let r = await converterFactory.registerTypedConverterFactory(liquidFactory.address);


		console.debug('registry', converterRegistry.address);
		console.debug('eth', etherToken.address);

		console.debug('token before', await converterRegistry.getAnchors());

		// TODO: move everything below here to next migration ste
		// wtf absolutely no way to get from this to something to divide by
		//const amount_bert = new bn(web3.eth.getBalance(accounts[3]).toNumber());
		//const amount_bert = web3.eth.getBalance(accounts[3]);
		//const amount_bert = web3.eth.getBalance(accounts[3]).toNumber();
		accounts = await web3.eth.getAccounts();
		//const amount_bert = new bn(await web3.eth.getBalance(accounts[2]));
		console.debug('accounts', accounts);
		const amount_bert = amount;
		console.debug('typeof', typeof amount_bert);
		console.debug('foo', amount_bert); //, amount_bert_reserve_factor_percentage, amount_bert * amount_bert_reserve_factor_percentage, parseInt(amount_bert * amount_bert_reserve_factor_percentage));
		console.debug('bert balance', accounts[2], await web3.eth.getBalance(accounts[2]));
		console.debug('ernie balance', accounts[3], await web3.eth.getBalance(accounts[3]));
		//let amount_bert_reserve = amount_bert.div(amount_bert_reserve_factor_percentage);
		let amount_bert_reserve = amount_bert.div(amount_bert_reserve_factor_percentage);
		r = await web3.eth.sendTransaction({
			from: accounts[2],
			to: etherToken.address,
			value: amount_bert_reserve,
		});
		
		//const amount_ernie = new bn(await web3.eth.getBalance(accounts[3]));
		const amount_ernie = amount;
		//let amount_ernie_reserve = amount_ernie.div(amount_ernie_reserve_factor_percentage);
		let amount_ernie_reserve = amount_ernie.div(amount_ernie_reserve_factor_percentage);
		r = await web3.eth.sendTransaction({
			from: accounts[3],
			to: etherToken.address,
			value: amount_ernie_reserve,
		});
		amount_bert_reserve = await etherToken.balanceOf(accounts[2]);
		console.debug('bert spends ' + amount_bert_reserve.toString() + ' of ' + amount_bert);
		amount_ernie_reserve = await etherToken.balanceOf(accounts[3]);
		console.debug('ernie spends ' + amount_ernie_reserve.toString() + ' of ' + amount_ernie);

		await converterRegistry.newConverter(
			0,
			'Bert Token',
			'BRT',
			18,
			100000,
			[etherToken.address],
			[250000],
			{
				from: accounts[2],
			}
		);
		await converterRegistry.newConverter(
			0,
			'Ernie Token',
			'RNI',
			18,
			100000,
			[etherToken.address],
			[250000],
			{
				from: accounts[3],
			}
		);

		// smart token objects
		const as = await converterRegistry.getAnchors()

		const bertToken = await SmartToken.at(as[0]);
		console.debug('bert token', as[0], await bertToken.symbol(), await bertToken.owner(), await bertToken.totalSupply());
		const ernieToken = await SmartToken.at(as[1]);
		console.debug('ernie token', as[1], await ernieToken.symbol(), await ernieToken.owner(), await ernieToken.totalSupply());

		// converter objects
		const cs = await converterRegistry.getConvertersByAnchors(as);

		const bertConverter = await LiquidTokenConverter.at(cs[0]);
		console.debug('bert converter', cs[0], await bertConverter.owner(), await bertConverter.reserveBalance(etherToken.address));

		const ernieConverter = await LiquidTokenConverter.at(cs[1]);
		console.debug('ernie converter', cs[1], await ernieConverter.owner());


		// perform conversions
		const reserveToken = await ERC20Token.at(etherToken.address);
		console.debug('foo');
		await reserveToken.approve(bancorNetwork.address, 0, {
			from: accounts[2],
		});
		console.debug('bar');
		await reserveToken.approve(bancorNetwork.address, amount_bert_reserve, {
			from: accounts[2],
		});
		await bancorNetwork.convert([
			etherToken.address,
			bertToken.address,
			bertToken.address,
		],
			amount_bert_reserve,
			amount_bert_reserve, {
			from: accounts[2],
		});

		await reserveToken.approve(bancorNetwork.address, 0, {
			from: accounts[3],
		});
		await reserveToken.approve(bancorNetwork.address, amount_ernie_reserve, {
			from: accounts[3],
		});
		await bancorNetwork.convert([
			etherToken.address,
			ernieToken.address,
			ernieToken.address,
		],
			amount_ernie_reserve,
			amount_ernie_reserve, {
			from: accounts[3],
		});

		// expect balance 1:4 but 0.6.8 always does initial supply 1:1 to reserve.
		// we need to wait for next version
		reserve_balance_bert_converter = await bertConverter.reserveBalance(etherToken.address);
		reserve_weight_bert_converter = await bertConverter.reserveWeight(etherToken.address);
		console.debug('bert converter', cs[0], await bertConverter.owner(), reserve_balance_bert_converter.toString(), reserve_weight_bert_converter.toString());
	
		supply_bert = await bertToken.totalSupply();
		const liquid_balance_bert = await bertToken.balanceOf(accounts[2])
		console.debug('bert token', as[0], await bertToken.symbol(), await bertToken.owner(), supply_bert.toString(), liquid_balance_bert.toString());

		reserve_balance_ernie_converter = await ernieConverter.reserveBalance(etherToken.address);
		reserve_weight_ernie_converter = await ernieConverter.reserveWeight(etherToken.address);
		console.debug('ernie converter', cs[1], await ernieConverter.owner(), reserve_balance_ernie_converter.toString(), reserve_weight_ernie_converter.toString());
	
		supply_ernie = await ernieToken.totalSupply();
		const liquid_balance_ernie = await ernieToken.balanceOf(accounts[3])
		console.debug('ernie token', as[1], await ernieToken.symbol(), await ernieToken.owner(), supply_ernie.toString(), liquid_balance_ernie.toString());


		//reserve_balance_ernie = await reserveToken.balanceOf(accounts[3]);
		console.debug('reserves', amount_bert_reserve.toString(), amount_ernie_reserve.toString());

		// output is used for CIC platform development deployments
		token_output = {
			registry: {
				address: contractRegistry.address,
				deployer: accounts[0],
			},
			reserve: {
				name: await reserveToken.name(),
				symbol: await reserveToken.symbol(),
				decimals: await reserveToken.decimals(),
				address: await reserveToken.address,
				deployer: accounts[1],
				balances: {
				},
			},
			tokens: [
				{
					converter: await bertConverter.address,
					weight: 250000,
					name: await bertToken.name(),
					symbol: await bertToken.symbol(),
					decimals: await bertToken.decimals(),
					address: await bertToken.address,
					deployer: accounts[2],
					deployer_balance: liquid_balance_bert,
				},
				{
					converter: await ernieConverter.address,
					weight: 250000,
					name: await ernieToken.name(),
					symbol: await ernieToken.symbol(),
					decimals: await ernieToken.decimals(),
					address: await ernieToken.address,
					deployer: accounts[3],
					deployer_balance: liquid_balance_ernie,
				},
			],
		};
		token_output.reserve.balances[accounts[2]] = amount_bert_reserve;
		token_output.reserve.balances[accounts[3]] = amount_ernie_reserve;

		fs.writeFileSync('tokens.json', JSON.stringify(token_output));
	});
};
