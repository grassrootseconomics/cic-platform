from enum import Enum

class RegistrationMethodEnum(Enum):
    USSD_SIGNUP = 'USSD_SIGNUP'
    WEB_SIGNUP = 'WEB_SIGNUP'
