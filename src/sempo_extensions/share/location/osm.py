osm_extension_fields = {
    'osm_id',
    'class',
    'osm_type',
        }


def valid_data(data):
    if not isinstance(data, dict):
        return False
    for field in osm_extension_fields:
        if data.get(field) == None:
            return False
    return True
