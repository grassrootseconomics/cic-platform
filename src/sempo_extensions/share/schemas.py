# third party imports
from marshmallow import fields

# platform imports
from server.schemas import UserSchema


def get_ussd_menu_info_title(self, obj):
        if obj.ussd_menu_info_title_configs:
            return obj.ussd_menu_info_title_configs['info_title']

class ExtendedUserSchema(UserSchema):
    registration_method     = fields.Function(lambda obj: obj.registration_method.value)
    ussd_menu_info_title    = fields.Method("get_ussd_menu_info_title")
