# framework imports
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship

# platform imports
import config
from server import db, bt
from server.models import user as base_user
from share.models.user_extension import UserExtension
from share.models.location import Location
from share.user.enum import RegistrationMethodEnum


class ExtendedUser(base_user.User):

    __mapper_args__ = {
        'polymorphic_identity':'extended_user',
    }
    
    _full_location = db.relationship(Location,
            secondary='user_extension_association_table')

    registration_method = db.Column(db.Enum(RegistrationMethodEnum))

    @hybrid_property
    def full_location(self):
        return self._full_location[0]

    def set_full_location(self, location):
        self._full_location.clear()
        self._full_location.append(location)

    def get_base_user(self):
        return super(ExtendedUser, self)
    
    def get_full_location(self):
        return self._full_location

    def __init__(self, blockchain_address=None, **kwargs):
        super(ExtendedUser, self).__init__(blockchain_address, **kwargs)
        self.registration_method = kwargs['registration_method']


class UssdUser(ExtendedUser):

    ussd_menu_info_title_configs = db.Column(JSONB)

    def is_pin_reset_token_valid(self, pin_reset_token):
        self.clear_expired_pin_reset_tokens()
        pin_reset_token_in_valid_reset_tokens = pin_reset_token in self.pin_reset_tokens
        return pin_reset_token_in_valid_reset_tokens

    def user_details(self):
        # return phone numbers only if any of user's details are unknown
        if 'Unknown' in self.first_name or 'Unknown' in self.last_name:
            return "{}".format(self.phone)
        return "{} {} {}".format(self.first_name, self.last_name, self.phone)

    def set_ussd_menu_info_title(self,
        info_title,
        sessions_visible=config.USSD_INFO_TITLE_DISPLAY_SESSIONS):
        """
        This method persists the custom info title and the number of ussd sessions for which the custom info title will
        be visible.
        :param info_title: The message to be added on the ussd menu info title.
        :param sessions_visible: number of times the info title will be visible.
        :return:
        """
        self.ussd_menu_info_title_configs = {
            'info_title': info_title,
            'sessions_visible': sessions_visible
        }


    def __init__(self, blockchain_address=None, **kwargs):
        if self.registration_method != RegistrationMethodEnum.USSD_SIGNUP:
            self.primary_blockchain_address = blockchain_address or bt.create_blockchain_wallet()
        blockchain_address = self.primary_blockchain_address
        super(UssdUser, self).__init__(blockchain_address, **kwargs)
