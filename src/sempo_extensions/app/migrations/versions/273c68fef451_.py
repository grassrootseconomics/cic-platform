"""leftover failed incremental change of column type

Revision ID: 273c68fef451
Revises: 4418fe7ee9d0
Create Date: 2020-06-26 09:09:53.035804

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '273c68fef451'
down_revision = '4418fe7ee9d0'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
