"""empty message

Revision ID: 7e275774f175
Revises: fbd0f6a88346
Create Date: 2020-10-12 18:19:12.338102

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7e275774f175'
down_revision = 'fbd0f6a88346'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("notification", "status_serial", server_default='0')
    pass


def downgrade():
    pass
