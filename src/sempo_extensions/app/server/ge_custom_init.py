from server.api2.geolocation_legacy_api import geolocation_legacy_blueprint
from server.api2.geolocation_api import geolocation_blueprint
from server.api2.user_api import user_blueprint
from server.api2.notification_api import notification_blueprint
from server.api2.exchange_api import exchange_blueprint
from server.api2.transfer_account_api import transfer_account_blueprint

def do(app=None):
    api_prefix = '/api/v2'
    app.register_blueprint(geolocation_legacy_blueprint, url_prefix=api_prefix)
    app.register_blueprint(geolocation_blueprint, url_prefix=api_prefix)
    app.register_blueprint(user_blueprint, url_prefix=api_prefix)
    app.register_blueprint(notification_blueprint, url_prefix=api_prefix)
    app.register_blueprint(exchange_blueprint, url_prefix=api_prefix)
    app.register_blueprint(transfer_account_blueprint, url_prefix=api_prefix)
