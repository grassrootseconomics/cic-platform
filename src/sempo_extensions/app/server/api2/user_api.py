# standard imports
import logging

# third party imports
from flask import Blueprint, request, make_response, jsonify, g
from flask.views import MethodView

# platform imports
from server import db
from server.models.user import User
from share.models.user import ExtendedUser
from server.utils.auth import requires_auth, show_all
from server.utils.access_control import AccessControl
import share.user.extended_user as extended_user
from server.utils.registration import create_user

logg = logging.getLogger()


class UserLocationAPI(MethodView):
    """Handles location-related operations on user

    Documentation is brief since it implmements standard flask blueprint interface
    """

    @requires_auth
    def get(self, user_id):
        """gets the stored location for the given user
        """
        if not AccessControl.has_suffient_role(g.user.roles, {'ADMIN': 'subadmin'}):
            return make_response(jsonify({
                'message': 'no clearance, clarence',
                }), 403)

        u = ExtendedUser.query.get(user_id)
        logg.debug('user {} -> {}'.format(user_id, u))

        response_object={
            'user_id': user_id,
            'location': [],
        }
        location = u.full_location
        while location != None:
            response_object['location'].append({
                    'latitude': location.latitude,
                    'longitude': location.longitude,
                    'common_name': location.common_name,
                })
            location = location.parent

        return make_response(jsonify(response_object)), 200


    @requires_auth
    def put(self, user_id):
        if not AccessControl.has_suffient_role(g.user.roles, {'ADMIN': 'admin'}):
            return make_response(jsonify({
                'message': 'no clearance, clarence',
                }), 403)

        request_data = request.get_json()
        request_data['user_id'] = user_id

        #try:
        extended_user.update(user_id, request_data)
        #except:
        #    return make_response(jsonify({'message': 'invalid update data'})), 400
                
        # TODO: load full user data and return that
        return make_response(jsonify(request_data)), 204


class UserRegisterAPI(MethodView):

    def post(self):
        organisation = g.get('active_organisation')
        request_data = request.get_json()

        phone = request_data['phone']

        new_user = None
        try:
            new_user = create_user(
                    phone,
                    organisation,
                    first_name = request_data.get('first_name'),
                    last_name = request_data.get('last_name'),
                    )
        except ValueError as e:
            logg.error(e)
            return make_response(jsonify({'message': 'Organisation must be set'})), 400

        db.session.add(new_user)
        db.session.commit()
        return make_response(jsonify({'message': 'success',
            'data': {
                'id': new_user.id,
                }
            })), 201


class UserIdAPI(MethodView):
    """
    Receives blockchain address from the ussd standalone app and returns the matching user's primary key from the
    user's table.
    """
    @show_all
    def get(self, address):
        if address:
            user = User.query.filter_by(primary_blockchain_address=address).first()
            if user:
                response = {
                    'data': {
                        'user_id': user.id
                    },
                    'message': 'User ID loaded successfully.'
                }
                return make_response(jsonify(response), 200)
            else:
                response = {
                    'message': f'No user matching blockchain address: {address} was found.'
                }
                return make_response(jsonify(response), 404)
        else:
            response = {
                'message': 'No blockchain address provided.'
            }
            return make_response(jsonify(response), 400)


user_blueprint = Blueprint('v2_user_extensions', __name__)

user_blueprint.add_url_rule(
        '/user/<int:user_id>/geolocation/',
        view_func=UserLocationAPI.as_view('v2_user_geo|ocation_view'),
        methods=['GET', 'PUT'],
        )

user_blueprint.add_url_rule(
        '/user/',
        view_func=UserRegisterAPI.as_view('v2_user_registration_view'),
        methods=['POST'],
        )

user_blueprint.add_url_rule(
        '/user_id/eth/<string:address>',
        view_func=UserIdAPI.as_view('v2_user_id_from_blockchain_view'),
        methods=['GET']
        )
