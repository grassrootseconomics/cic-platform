from flask import Blueprint, make_response, jsonify
from flask.views import MethodView

from web3 import Web3

import config

eth_blueprint = Blueprint('eth', __name__)


# this is a temporary return of what should be parsed from bancor contract contracts/utility/ContractRegistryClient
class EthAPI(MethodView):
    def get(self):
        r = {
            'registry': config.ETH_CONTRACT_ADDRESS,
            'src': [
                'https://holbrook.no/ge_registry.txt',
                'dns://holbrook.no/txt/_ge_registry',

                ]
                }
        return make_response(jsonify(r))


eth_blueprint.add_url_rule(
    '/eth/',
    view_func=EthAPI.as_view('eth_api'),
    methods=['GET']
)

