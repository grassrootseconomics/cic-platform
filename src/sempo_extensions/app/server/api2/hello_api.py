from flask import Blueprint, make_response, jsonify
from flask.views import MethodView

hello_blueprint = Blueprint('hello', __name__)

class HelloAPI(MethodView):

    def get(self):
        return(make_response(jsonify({'foo': 'bar'})))

hello_blueprint.add_url_rule(
    '/hello/',
    view_func=HelloAPI.as_view('hello_api'),
    methods=['GET']
)
