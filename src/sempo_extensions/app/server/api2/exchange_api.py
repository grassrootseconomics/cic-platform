import json
import logging

logg = logging.getLogger(__name__)

from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView

exchange_blueprint = Blueprint('exchange', __name__)

from server import db
from server.utils.auth import requires_auth
from server.models.exchange import ExchangeContract
from server.models.token import Token
from server.models.organisation import Organisation
from server.schemas import exchange_contract_schema

class ExchangeContractAPI(MethodView):
    @requires_auth
    def post(self):
        post_data = request.get_json()
        reserve_address = post_data['reserve_address']
        token_address = post_data['token_address']
        exchange_address = post_data['exchange_address']
        registry_address = post_data['registry_address']
        reserve_ratio_ppm = post_data['reserve_ratio_ppm']
        organisation_id = post_data['organisation_id']

        q = db.session.query(Token)
        q = q.filter(
            Token.address==reserve_address,
        )
        logg.debug("reserve sql {}".format(q))
        reserve_token = q.first()

        q = db.session.query(Token)
        q = q.filter(
            Token.address==token_address,
        )
        logg.debug("token sql {}".format(q))
        token = q.first()
        if token == None:
            response_object = {
                'message': 'CIC Token not found',
                'data': {
                    'token_address': token_address
                }
            }
            return make_response(jsonify(response_object)), 400
        if reserve_token == None:
            response_object = {
                'message': 'Reserve token not found',
                'data': {
                    'token_address': reserve_address
                }
            }
            return make_response(jsonify(response_object)), 400
        exchange_contract = ExchangeContract(
                reserve_token_id=token.id,
                blockchain_address=exchange_address,
                contract_registry_blockchain_address=registry_address,
                )

        # TODO: this should be active organisation for logged in user instead
        # TODO: Audit whether it actually makes sense to have org constraint here, exchange is per pair of token and converter, and organisation would be inferred from token if a one-to-one mapping is to be enforced.
        organisation = Organisation.query.get(organisation_id)
        #organisation = Organisation.master_organisation()
       
        # TODO: primary blockchain address - is this correct? there is a "system" one too, and of course the user's. Sooo many choices, so little documentation
        logg.debug('reserve {} token {} organisation {}'.format(reserve_token.address, token.address, organisation.primary_blockchain_address))
        db.session.add(exchange_contract)
        db.session.commit()
        exchange_contract.register_reserve_token(reserve_token)
        db.session.commit()
        exchange_contract.register_token(token, organisation.primary_blockchain_address, int(reserve_ratio_ppm))
        db.session.commit()

        response_object = {
            'message': 'Exchange Added',
            'data': {
                'exchange': exchange_contract_schema.dump(exchange_contract).data
            }
        }
        return make_response(jsonify(response_object)), 201

exchange_blueprint.add_url_rule(
    '/exchange/register/',
    view_func=ExchangeContractAPI.as_view('exchange_contract_view_ge'),
    methods=['POST']
)
