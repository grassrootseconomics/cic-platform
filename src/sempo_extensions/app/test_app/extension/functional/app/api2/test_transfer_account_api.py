from urllib.parse import quote
from server.utils.auth import get_complete_auth_token


def test_search_for_transfer_account_by_phone(test_client, create_transfer_account_user, authed_sempo_admin_user):
    """
    GIVEN a running instance of teh flask app
    WHEN a get requests with a valid phone number is sent to the '/api/v2/search_transfer_account/' endpoint
    THEN transfer account data matching the respective user's phone number is sent back
    """
    # get admin auth token
    auth = get_complete_auth_token(authed_sempo_admin_user)

    user = create_transfer_account_user
    search_transfer_account_response = test_client.get(
        f"/api/v2/search_transfer_account/?user_phone={quote(user.phone)}",
        headers=dict(
            Authorization=auth,
            Accept='application/json',
        )
    )
    assert search_transfer_account_response.status_code == 200
    transfer_account_result = search_transfer_account_response.json.get('data')['transfer_account']

    assert user.default_transfer_account.blockchain_address == transfer_account_result.get('blockchain_address')
