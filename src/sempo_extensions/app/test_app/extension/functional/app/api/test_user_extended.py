# standard imports
import json
import random

# third-party imports
import pytest
from faker import Faker

# platform imports
import config
from server import db
from server.utils.auth import get_complete_auth_token
from helpers.utils import make_kenyan_phone

# test imports
from extension.helpers.extended_factories import ExtendedUserFactory, LocationFactory


def test_get_user_location(
        test_client,
        init_database,
        create_organisation,
        authed_sempo_admin_user,
        new_locations,
        create_temporary_extended_user,
        ):
    """
    GIVEN an extended user record
    WHEN adding location to that user
    THEN the location is retrievable from the user location http api endpoint
    """

    # create organisation
    organisation = create_organisation
    organisation.external_auth_password = config.EXTERNAL_AUTH_PASSWORD

    # create admin
    admin = authed_sempo_admin_user
    admin.set_held_role('ADMIN', 'admin')

    # create user
    user = create_temporary_extended_user
    user.set_full_location(new_locations['leaf'])
    db.session.commit()

    user.get_base_user().add_user_to_organisation(organisation, False)

    # get admin auth token
    auth = get_complete_auth_token(authed_sempo_admin_user)

    response = test_client.get(
            '/api/v2/user/{}/geolocation/'.format(user.id),
            headers=dict(
                Authorization=auth,
                Accept='application/json',
                ),
            )
    assert response.status_code == 200

    i = 0
    for k in ['leaf', 'node', 'top']:
        assert response.json['location'][i]['latitude'] == new_locations[k].latitude
        assert response.json['location'][i]['longitude'] == new_locations[k].longitude
        assert response.json['location'][i]['common_name'] == new_locations[k].common_name
        i += 1


def test_set_user_location(
        test_client,
        init_database,
        create_organisation,
        authed_sempo_admin_user,
        new_locations,
        create_temporary_extended_user,
        ):


    # create organisation
    organisation = create_organisation
    organisation.external_auth_password = config.EXTERNAL_AUTH_PASSWORD

    # create admin
    admin = authed_sempo_admin_user
    admin.set_held_role('ADMIN', 'admin')

    # create user
    user = create_temporary_extended_user
    user.set_full_location(new_locations['leaf'])
    db.session.commit()

    user.get_base_user().add_user_to_organisation(organisation, False)

    # get admin auth token
    auth = get_complete_auth_token(authed_sempo_admin_user)

    assert user._location == None
    response = test_client.put(
            '/api/v2/user/{}/geolocation/'.format(user.id),
            headers=dict(
                Authorization=auth,
                Accept='application/json',
            ),
            content_type='application/json',
            follow_redirects=True,
            data=json.dumps({
                'location_id': new_locations['leaf'].id,
            }),
       )

    assert response.status_code == 204
    assert user.full_location == new_locations['leaf']
    assert user._location == new_locations['leaf'].common_name
    assert user.latitude == new_locations['leaf'].latitude
    assert user.longitude == new_locations['leaf'].longitude

valid_phone_prefixes = ['+254']

def make_valid_phone():
    fake = Faker()
    prefix_idx = random.randrange(len(valid_phone_prefixes))
    return ''.join([valid_phone_prefixes[prefix_idx], fake.msisdn()])


def test_register_bare_user(
        test_client,
        init_database,
        create_organisation,
        authed_sempo_admin_user,
        ):
        

    # create organisation
    organisation = create_organisation
    organisation.external_auth_password = config.EXTERNAL_AUTH_PASSWORD

    # get admin auth token
    auth = get_complete_auth_token(authed_sempo_admin_user)

    phone = make_valid_phone()

    response = test_client.post(
            '/api/v2/user/',
            headers=dict(
                Accept='application/json',
            ),
            content_type='application/json',
            follow_redirects=True,
            data=json.dumps({
                'phone': phone,
            }),
       )

    assert response.status_code == 201

    response = test_client.get(
            '/api/v1/user/{}/'.format(response.json['data']['id']),
            headers=dict(
                Authorization=auth,
                Accept='application/json',
            ),
            content_type='application/json',
            follow_redirects=True,
       )

    assert response.status_code == 200


def test_get_user_id_from_blockchain_address(
        test_client,
        init_database,
        create_organisation,
        create_user_with_existing_transfer_account):
    """
    GIVEN a running instance of the flask application
    WHEN a post request is sent to /api/v2/user_id_from_blockchain_address/ with a valid user's blockchain address
    THEN a corresponding user id is sent as a response
    """
    response = test_client.get(
        f'/api/v2/user_id/eth/{create_user_with_existing_transfer_account.primary_blockchain_address}'
    )

    user_id = response.json.get('data').get('user_id')
    assert user_id == create_user_with_existing_transfer_account.id

