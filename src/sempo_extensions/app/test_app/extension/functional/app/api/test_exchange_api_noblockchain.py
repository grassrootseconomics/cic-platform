# standard imports
import pytest
import logging

# platform imports
from server.utils.auth import get_complete_auth_token

# these values are totally arbitrary
converter_address = '0xF8A3501FBDAF3CE2F3B44C4A6F481FE9D8E34D44'
registry_address = '0xFE5784AF7C78A0BEDF87724D9DF3D29C8B985553'

logg = logging.getLogger(__file__)


def test_exchange_api(
        test_client,
        init_database,
        user_with_reserve_balance,
        create_reserve_token,
        create_liquid_token,
        create_master_organisation,
        ):

    response = test_client.post(
        '/api/v2/exchange/register/',
        headers = dict(
            Authorization = get_complete_auth_token(user_with_reserve_balance),
            Accept = 'application/json',
            ),
        json = {
            'reserve_address': create_reserve_token.address,
            'token_address': create_liquid_token.address,
            'exchange_address': converter_address,
            'registry_address': registry_address,
            'reserve_ratio_ppm': 250000,
            'organisation_id': create_master_organisation.id,
            },
        )

    assert response.status_code == 201
