import { combineReducers } from "redux";
import { addCreditTransferIdsToTransferAccount, DEEEEEEP } from "../../utils";

export const DEEP_UPDATE_SEARCHED_TRANSFER_ACCOUNT = "DEEP_UPDATE_SEARCHED_TRANSFER_ACCOUNT";
export const UPDATE_SEARCHED_TRANSFER_ACCOUNT_CREDIT_TRANSFERS =
  "UPDATE_SEARCHED_TRANSFER_ACCOUNT_CREDIT_TRANSFERS";
export const UPDATE_SEARCHED_TRANSFER_ACCOUNT = "UPDATE_SEARCHED_TRANSFER_ACCOUNT";

export const SEARCH_TRANSFER_ACCOUNT_REQUEST = 'SEARCH_TRANSFER_ACCOUNT_REQUEST'
export const SEARCH_TRANSFER_ACCOUNT_SUCCESS = 'SEARCH_TRANSFER_ACCOUNT_SUCCESS'
export const SEARCH_TRANSFER_ACCOUNT_FAILURE = 'SEARCH_TRANSFER_ACCOUNT_FAILURE'

const byId = (state = {}, action) => {
    switch (action.type) {
        case DEEP_UPDATE_SEARCHED_TRANSFER_ACCOUNT:
            return DEEEEEEP(state, action.transfer_accounts);

        case UPDATE_SEARCHED_TRANSFER_ACCOUNT_CREDIT_TRANSFERS:
            var newState = {};

            action.credit_transfer_list.map(transfer => {
                if (transfer.transfer_subtype === "DISBURSEMENT") {
                let updatedTransferAccount = {
                    [transfer.recipient_transfer_account.id]: {
                    credit_receives: [transfer.id]
                    }
                };
                newState = { ...newState, ...updatedTransferAccount };
                } else if (transfer.transfer_subtype === "RECLAMATION") {
                let updatedTransferAccount = {
                    [transfer.sender_transfer_account.id]: {
                    credit_sends: [transfer.id]
                    }
                };
                newState = { ...newState, ...updatedTransferAccount };
                }
            });

            return addCreditTransferIdsToTransferAccount(state, newState)

        case UPDATE_SEARCHED_TRANSFER_ACCOUNT:
            return action.transfer_account;
    
        default:
        return state;
    }
}

// define initial state for the search request
const initialSearchTransferAccountState = {
    isRequesting: false,
    success: false,
    error: null
}


// define reducer
const loadStatus = ( state = initialSearchTransferAccountState, action) => {
    switch (action.type){
        case SEARCH_TRANSFER_ACCOUNT_REQUEST:
            return {
                ...state,
                isRequesting: true
            }
        case SEARCH_TRANSFER_ACCOUNT_SUCCESS:
            return {
                ...state,
                isRequesting: false,
                success: true
            }
        case SEARCH_TRANSFER_ACCOUNT_FAILURE:
            return {
                ...state,
                isRequesting: false,
                error: action.error
            }
            
        default:
            return state
    }
}

export const searchedTransferAccount = combineReducers({
    loadStatus,
    byId
})

export const requestSearchTransferAccount = query => ({
    type: SEARCH_TRANSFER_ACCOUNT_REQUEST,
    query
})




