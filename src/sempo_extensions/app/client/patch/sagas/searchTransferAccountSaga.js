import { searchTransferAccountAPI } from "../api/searchTransferAccountAPI";
import { all, call, put, takeEvery } from "redux-saga/effects";
import { normalize } from "normalizr";
import {handleError} from "../../utils";
import {
    SEARCH_TRANSFER_ACCOUNT_SUCCESS,
    SEARCH_TRANSFER_ACCOUNT_FAILURE,
    SEARCH_TRANSFER_ACCOUNT_REQUEST,
    DEEP_UPDATE_SEARCHED_TRANSFER_ACCOUNT
} from "../reducers/searchTransferAccountReducer";
import { MessageAction } from "../../reducers/message/actions";
import { UserListAction } from "../../reducers/user/actions";
import { transferAccountSchema } from "../../schemas";
import {CreditTransferAction} from "../../reducers/creditTransfer/actions";

function* updateStateFromSearchedTransferAccount(data){

    if (data.transfer_account) {
        var transfer_account_list = [data.transfer_account];
    }
    const normalizedData = normalize(
        transfer_account_list,
        transferAccountSchema
    );

    const users = normalizedData.entities.users;
    if (users) {
        yield put(UserListAction.deepUpdateUserList(users));
    }

    const credit_sends = normalizedData.entities.credit_sends;
    if (credit_sends) {
        yield put(CreditTransferAction.updateCreditTransferListRequest(credit_sends));
    }

    const credit_receives = normalizedData.entities.credit_receives;
    if (credit_receives) {
        yield put(CreditTransferAction.updateCreditTransferListRequest(credit_receives));
    }

    var transfer_accounts = normalizedData.entities.transfer_accounts;
    if (transfer_accounts) {
        yield put({
            type: DEEP_UPDATE_SEARCHED_TRANSFER_ACCOUNT,
            transfer_accounts
        });
    }

}

function* fetchTransferAccount({ query }){
    try {
        // define result received
        const fetch_result = yield call(searchTransferAccountAPI, query)

        yield call(updateStateFromSearchedTransferAccount, fetch_result.data)

        yield put({
            type: SEARCH_TRANSFER_ACCOUNT_SUCCESS
        })

    } catch (fetch_error) {
        const error = yield call(handleError, fetch_error);
        yield put({
            type: SEARCH_TRANSFER_ACCOUNT_FAILURE,
            error
        })
        yield put(
            MessageAction.addMessage({ error: true, message: error.message })
          );

    }
}

function* watchSearchTransferAccount(){
    yield takeEvery(SEARCH_TRANSFER_ACCOUNT_REQUEST, fetchTransferAccount)
}

export default function* searchTransferAccountSaga() {
    yield all([watchSearchTransferAccount()])
}