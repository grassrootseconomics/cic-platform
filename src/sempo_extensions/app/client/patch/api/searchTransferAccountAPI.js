import {apiClient} from "../../api/client/apiClient";


export const searchTransferAccountAPI = ({ query }) =>
    apiClient({
        url: "/search_transfer_account/",
        method: "GET",
        query: query,
        apiVersionValue: 2
    });