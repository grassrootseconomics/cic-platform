import React from "react";
import { connect } from "react-redux";
import { requestSearchTransferAccount } from "../reducers/searchTransferAccountReducer";
import SearchBar from "./searchBar";
import { PageWrapper, WrapperDiv } from "../../components/styledElements"
import { ThemeProvider } from "styled-components";
import { LightTheme } from "../../components/theme";
import TransferAccountResult from "./transferAccountResult";

const mapStateToProps = state => {
    return {
        login: state.login,
        searchedTransferAccount: state.searchedTransferAccount,
        mergedTransferAccountUserList: Object.keys(state.searchedTransferAccount.byId)
        .map(id => {
            return {
            ...{
                id,
                ...state.users.byId[state.searchedTransferAccount.byId[id].primary_user_id]
            },
            ...state.searchedTransferAccount.byId[id]
            };
        }).filter(mergedObj => mergedObj.users && mergedObj.users.length >= 1), // only show mergedObjects with users
        users: state.users
    }
}

const mapDispatchToProps = dispatch => {
    return {
        requestSearchTransferAccount: query => dispatch(requestSearchTransferAccount({ query }))
    }
}


class SearchTransferAccountPage extends React.Component {
    constructor(props) {
        super(props);
        this.fetchSearchedTransferAccount = this.fetchSearchedTransferAccount.bind(this)
    }

    fetchSearchedTransferAccount(value){
        if (value !== undefined && value !== ""){
            this.props.requestSearchTransferAccount({ user_phone: value })
        }
    }

    render(){
        let searchedTransferAccountData = this.props.mergedTransferAccountUserList
        return(
            <PageWrapper>
                <SearchBar
                  fetchSearchedTransferAccount={ this.fetchSearchedTransferAccount }/>

                <ThemeProvider theme={LightTheme}>
                    <TransferAccountResult searchedTransferAccountData = {searchedTransferAccountData}/>
                </ThemeProvider>
            </PageWrapper>
        )
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchTransferAccountPage)

