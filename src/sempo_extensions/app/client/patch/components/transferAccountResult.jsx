import React from "react";
import { connect } from "react-redux"
import styled from "styled-components";
import ReactTable from "react-table";
import LoadingSpinner from "../../components/loadingSpinner";
import { ModuleBox, Wrapper } from "../../components/styledElements";
import DateTime from "../../components/dateTime";
import { formatMoney } from "../../utils";
import { browserHistory } from "../../createStore";

const mapStateToProps = state => {
    return {
        login: state.login,
        searchedTransferAccount: state.searchedTransferAccount,
        creditTransfers: state.creditTransfers
    }
}

class TransferAccountResult extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
        }
        this.handleChange = this.handleChange.bind(this)
    }

    _customName(transferAccount) {
        if (
          this.props.login.adminTier === "view" &&
          typeof transferAccount.blockchain_address !== "undefined"
        ) {
          return transferAccount.blockchain_address;
        }
        return (
          (transferAccount.first_name === null ? "" : transferAccount.first_name) +
          " " +
          (transferAccount.last_name === null ? "" : transferAccount.last_name)
        );
    }

    _customIcon(transferAccount) {
        let url = "/static/media/user.svg";
        if (transferAccount.is_beneficiary) {
          url = "/static/media/user.svg";
        } else if (transferAccount.is_vendor) {
          url = "/static/media/store.svg";
        } else if (transferAccount.is_groupaccount) {
          url = "/static/media/groupaccount.svg";
        } else if (transferAccount.is_tokenagent) {
          url = "/static/media/tokenagent.svg";
        }
        return <UserSVG src={url} />;
    }

    handleChange(event){
        this.setState({ [event.target.name ]: event.target.value })
    }

    render(){

        const loadingStatus = this.props.searchedTransferAccount.loadStatus.isRequesting
        var transferAccountData = this.props.searchedTransferAccountData.sort((a, b) => b.id - a.id);

        if (this.props.searchedTransferAccount.loadStatus.isLoading) {
            return (
                <div style={{ display: "flex", justifyContent: "center", margin: "1em" }}>
                    <LoadingSpinner/>
                </div>
            )
        }

        if (this.props.searchedTransferAccount.loadStatus.success &&
            transferAccountData !== null &&
            transferAccountData !== undefined) {
            return(
                <div style={{ display: "flex", flexDirection: "column" }}>
                    <ModuleBox style={{ width: "calc(100% - 2em)" }}>
                        <Wrapper>
                            <ReactTable
                            columns={[
                                {
                                    Header: "",
                                    id: "transferAccountIcon",
                                    accessor: transferAccount =>
                                      this._customIcon(transferAccount),
                                    headerClassName: "react-table-header",
                                    width: 40,
                                    sortable: false
                                  },
                                  {
                                    Header: "Name",
                                    id: "transferAccountName",
                                    accessor: transferAccount =>
                                      this._customName(transferAccount),
                                    headerClassName: "react-table-header",
                                    className: "react-table-first-cell"
                                  },
                                  {
                                    Header: "Created",
                                    accessor: "created",
                                    headerClassName: "react-table-header",
                                    Cell: cellInfo => < DateTime created={cellInfo.value} />
                                  },
                                  {
                                    Header: "Balance",
                                    accessor: "balance",
                                    headerClassName: "react-table-header",
                                    Cell: cellInfo => {
                                      const token =
                                        cellInfo.original.token &&
                                        cellInfo.original.token.symbol;
                                      const money = formatMoney(
                                        cellInfo.value / 100,
                                        undefined,
                                        undefined,
                                        undefined,
                                        token
                                      );
                                      return <p style={{ margin: 0 }}>{money}</p>;
                                    }
                                  },
                                  {
                                    Header: "Status",
                                    accessor: "is_approved",
                                    headerClassName: "react-table-header",
                                    Cell: cellInfo => (
                                      <p style={{ margin: 0, cursor: "pointer" }}>
                                        {cellInfo.value === true ? "Approved" : "Unapproved"}
                                      </p>
                                    )
                                  }
                                ]}
                                loading={loadingStatus}
                                data={transferAccountData}
                                pageSize={5}
                                sortable={true}
                                showPagination={true}
                                showPageSizeOptions={false}
                                className="react-table"
                                resizable={false}
                                getTrProps={(state, rowInfo, instance) => {
                                  if (rowInfo) {
                                    return {
                                      style: {
                                        cursor: rowInfo.row ? "pointer" : null
                                      }
                                    }
                                }
                                return {};
                                }}
                                getTdProps={(state, rowInfo, column) => {
                                return {
                                  onClick: (e, handleOriginal) => {
                                    if (rowInfo && rowInfo.row) {
                                      browserHistory.push("/accounts/" + rowInfo.row._original.id);
                                    }
                                    if (handleOriginal) {
                                      handleOriginal();
                                    }
                                  }
                                };
                              }}
                            />
                        </Wrapper>
                    </ModuleBox>
                </div>
            )
        }

        return (
            <ModuleBox>
              <p style={{ padding: "1em", textAlign: "center" }}>
                  No transfer account
              </p>
            </ModuleBox>
        );
    }

}

export default connect(
    mapStateToProps,
    null
)(TransferAccountResult)

const UserSVG = styled.img`
cursor: pointer;
width: 20px;
height: 20px;
`;