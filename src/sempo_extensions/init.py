"""Sync all internal and external resources before start.

TODO: move file fetching from config.py in here
"""

# standard imports
import sys
import os
import logging

# platform imports
import config
#from files.gitlab import Gitlab
from files.git import Git

logg = logging.getLogger(__file__)

wd = os.path.abspath(os.path.dirname(__file__))


def to_environ_arg(arg):
    argu = arg.upper()
    eargs = {}

    eargs['engine'] = os.environ.get('CIC_FILES_{}_ENGINE'.format(argu))
    if eargs['engine'] == None:
        raise ValueError('engine not set: {}'.format(argu))
   
    eargs['remote_location'] = os.environ.get('CIC_FILES_{}_REMOTE_LOCATION'.format(argu))
    if eargs['remote_location'] == None:
        raise ValueError('location not set: {}'.format(argu))
    eargs['remote_path'] = os.environ.get('CIC_FILES_{}_REMOTE_PATH'.format(argu), '/')

    entries = os.environ.get('CIC_FILES_{}_ENTRIES'.format(argu))
    if entries == None:
        raise ValueError('entries not set: {}'.format(argu))

    eargs['entries'] = []
    for entry in entries.split(','):
        remote = ''
        local = ''
        if entry.find(':') > -1:
            (remote, local) = entry.split(':')
        else:
            remote = entry
            local = entry
        eargs['entries'].append((remote, local))

    eargs['username'] = os.environ.get('CIC_FILES_{}_USERNAME'.format(argu))
    eargs['password'] = os.environ.get('CIC_FILES_{}_PASSWORD'.format(argu))
    eargs['local_path'] = os.environ.get('CIC_FILES_{}_LOCAL_PATH'.format(argu), wd)
    if not os.path.isabs(eargs['local_path']):
        eargs['local_path'] = os.path.join(wd, eargs['local_path'])

    return eargs


def init(names):
    """Initializes the system

    Raises
    ------
    Exception 
        Any exception raised should result in immediate termination
    """

    logg.debug('initializing with files identifiers: {}'.format(names))
    for i in range(len(names)):
        earg = to_environ_arg(names[i])
        logg.debug('executing file retrieval: {}'.format(earg))
        locale_syncer = None
        kw = {}
        if earg['engine'] == 'git':

            if earg['username'] != None:
                kw['username'] = earg['username']
                kw['password'] = earg['password']
            locale_syncer = Git(source_path=earg['remote_location'], destination_path=earg['local_path'], source_sub_path=earg['remote_path'], **kw)
        else:
            raise ValueError('unknown file retrieval engine "{}"'.format(earg['engine']))

        r = locale_syncer.sync(earg['entries'])

        for f in r:
            logg.info('synced locale file: {}'.format(f))


# In certain cases, for example a docker invocation script,
# init may be called directly from the command line
if __name__ == "__main__":
    init(sys.argv[1:])
