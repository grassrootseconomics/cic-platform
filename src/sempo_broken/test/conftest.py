# third-party imports
import pytest

# platform imports
import config
from server import g, db
from server.utils.user import create_user_without_transfer_account


@pytest.fixture(scope='module')
def create_temporary_user(test_client, init_database, create_organisation):
    # create organisation
    organisation = create_organisation
    organisation.external_auth_password = config.EXTERNAL_AUTH_PASSWORD

    # set active organisation
    g.active_organisation = organisation

    # create user without a transfer account
    temp_user = create_user_without_transfer_account(unregistered_user_phone)
    db.session.add(temp_user)
    db.session.commit()

    return temp_user
