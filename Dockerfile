FROM python:3.6-slim-stretch

RUN apt update \
  && apt -y install gcc g++ libffi-dev libstdc++-6-dev python3-dev musl-dev libssl-dev curl

# GE MIGRATION
# For local OSX try:
# https://stackoverflow.com/questions/49025594/osx-ld-library-not-found-for-lssl
RUN apt -y install mysql-server default-libmysqlclient-dev

COPY ./contrib/sempo/app/requirements.txt /
COPY ./contrib/sempo/test/requirements.txt /test_requirements.txt

RUN cd / && pip install -r requirements.txt && pip install -r test_requirements.txt

RUN apt remove --purge -y gcc g++ libffi-dev libstdc++-6-dev python3-dev musl-dev libssl-dev

COPY ./contrib/sempo/config.py /src/
COPY ./contrib/sempo/config_files /src/config_files
ADD ./contrib/sempo/app/server /src/server
ADD ./contrib/sempo/app/migrations /src/migrations
ADD ./contrib/sempo/app/manage.py /src
ADD ./contrib/sempo/app/RedisQueue.py /src
ADD ./src/sempo/app/_docker_app_script.sh /
ADD ./contrib/sempo/test /src/test
ADD ./contrib/sempo/invoke_tests.py /src
#ADD ./contrib/sempo/coveragerc /src

COPY ./src/sempo_extensions/init.py /src/
COPY ./src/sempo_extensions/files /src/files
COPY ./src/sempo_extensions/share /src/share

WORKDIR /

RUN chmod +x /_docker_app_script.sh

ARG GIT_HASH
ENV GIT_HASH=$GIT_HASH

EXPOSE 9000

CMD ["/_docker_app_script.sh"]
