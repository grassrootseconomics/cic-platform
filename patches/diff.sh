#!/usr/bin/env bash

LC_ALL=C
diff -aur contrib/sempo src/sempo_patch | grep -v '^Only in' > patches/sempo.patch
# diff -aur contrib/sempo/docker-compose.yml docker-compose.yml | grep -v '^Only in' > patches/docker-compose.patch
diff -aur contrib/ussd src/ussd_patch | grep -v '^Only in' > patches/ussd.patch
