#!/usr/bin/env bash

echo 'Usage: override.sh $FROM $TO with full path name'

# TODO: improve detect repo root

FROM=`realpath $1`
TO=`realpath $2`

pushd $FROM
pwd
files=`find . -type f -name "*.py" -o -name "*.js" -o -name "*.jsx" -o -name "*.ts" -o -name "*.tsx" -o -name "*.ini" -o -name "*.svg" -o -name "*.png" -o -name "*.xml"| sed -e "s/^\.\///g"`
for f in ${files[@]}; do
	>&2 echo "checking $TO $f"
	if [ -f $TO/$f ]; then
		>&2 echo "file $f already exists in repo" && exit 1;
	fi
done
for f in ${files[@]}; do
	d=`dirname $f`
	if [ ! -d $TO/$d ]; then
		mkdir -vp $TO/$d
	fi
	cp -v $f $TO/$f
done

mkdir -p $SEMPO_PATH/config_files/secret
files=`find . -type f -name "*.ini" | sed -e "s/^\.\///g"`
for f in ${files[@]}; do
	>&2 echo "checking $TO $f"
	if [ -f $TO/$f ]; then
		>&2 echo "file $f already exists in repo" && exit 1;
	fi
done
for f in ${files[@]}; do
	d=`dirname $f`
	if [ ! -d $TO/$d ]; then
		mkdir -vp $TO/$d
	fi
	cp -v $f $TO/$f
done
popd
