{ }:

# nix-build default.nix -A env | cachix push sarafu-dev
# cachix use sarafu-dev
# Note: If using Darwin, replace sarafu-dev with ge-dev-darwin

let
  commitRev = "025deb80b2412e5b7e88ea1de631d1bd65af1840";
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/${commitRev}.tar.gz";
    sha256 = "09mp6vqs0h71g27w004yrz1jxla31ihf18phw69wj61ix74ac4m0";
  };
  pkgs = import nixpkgs { config = {}; };
  inherit (pkgs.lib) optional optionals;
  python = (let
    override = let
      packageOverrides = self: super: {
        # https://github.com/NixOS/nixpkgs/pull/92802/files
        # Override to disable tests for Darwin error `OSError: AF_UNIX path too long`
        scipy = super.scipy.overridePythonAttrs (oldAttrs: rec {
          doCheck = !pkgs.stdenv.isDarwin;
        });
        numpy = super.numpy.overridePythonAttrs (oldAttrs: rec {
          doCheck = !pkgs.stdenv.isDarwin;
        });
        pandas = super.pandas.overridePythonAttrs (oldAttrs: rec {
          doCheck = !pkgs.stdenv.isDarwin;
        });
      };
  in pkgs.python36.override {inherit packageOverrides; self = override;};
  in override.withPackages (pkgs: with pkgs; [
    pip
    setuptools
    virtualenv
    numpy
    pandas
  ]));
  pythonPackages = python.pkgs;
  ganache = pkgs.callPackage ./nix/ganache/default.nix { inherit (pkgs); };
in
{
  inherit pkgs python pythonPackages;

  env = pkgs.buildEnv {
    name = "ge-default";
    paths = with pkgs; [
      python
      git
      bash
      kubectl
      docker
      docker_compose
      nodejs-13_x
      redis
      postgresql_12
      #pgadmin
      ncurses
      ganache.ganache-cli
      libmysqlclient
      gcc
      curl
      ps
      cacert
      jq
    ] ++ optionals stdenv.isDarwin (lib.attrValues darwin.apple_sdk.frameworks);
  };
}
