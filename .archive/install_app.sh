#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# GPG: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
# Description: Create a 100% clean deployment of the sarafu platform for use in development
#
# Script is not in safe state and may not exit cleanly; you might have to manually kill celery and ganache

# arg #1 is the path to the bancor directory. if set, must be one.
if [ ! -z "$1" ]; then
	if [ ! -d "$1" ]; then
		>&2 echo arg 2 must be a directory
		exit 1
	fi
fi


# import the necessary environment vars
. quick_env.sh


# check if necessary services are running
which netcat
if [ $? -eq 0 ]; then
	netcat -z $DATABASE_HOST $DATABASE_PORT
	if [ $? -gt 0 ]; then
		>&2 echo "tcp port $DATABASE_HOST:$DATABASE_PORT not responding. Is postgresql running?"
	fi
	#netcat -z $WEB3_HOST $WEB3_PORT
	#if [ $? -gt 0 ]; then
	#	>&2 echo "tcp port $WEB3_HOST:$WEB3_PORT not responding. Is ganache-cli running?"
	#fi
	netcat -z $REDIS_HOST $REDIS_PORT
	if [ $? -gt 0 ]; then
		>&2 echo "tcp port $REDIS_HOST:$REDIS_PORT not responding. Is redis running?"
	fi
fi



# kill already running subprocesses invoked by earlier runs of this script
for p in `find $d_run -iname '*.pid'`; do
	pp=`cat $p`
	echo "killing pid $pp"
	kill -TERM $pp
done


# add convenince vars for the work and config directories
# TODO: mkdirs are unnecessary because file structure should have been set up with the bootstrap script
d=`realpath $(dirname ${BASH_SOURCE[0]})`
d_log=$d/var/log/grassroots
d_lib=$d/var/lib/grassroots
d_run=$d/var/run/grassroots
d_tools=$d/bin
mkdir -vp $d_log
mkdir -vp $d_lib
mkdir -vp $d_run
sempodir=$SEMPO_PATH
sempooverridedir=$SEMPO_OVERRIDE_APP_PATH
export PYTHONPATH=$sempodir:$sempodir/eth_worker/eth_manager:$sempodir/eth_worker:$sempodir/eth_worker/eth_manager/task_interfaces:$PYTHONPATH


# verify tool dependencies
deps=(jq)
node_bin=`which node`
if [ "$?" -gt 0 ]; then
	>&2 echo "node not found"
	exit 1
fi
node_version=`node --version`
node_version_bancor="10.16.0"
if [ "$node_version" != "v${node_version_bancor}" ]; then
	nvm_bin=`which nvm`
	if [ $? -gt 0 ]; then
		>&2 echo "bancor needs node version $node_version_bancor. Maybe nvm can help you out?"
		exit 1	
	fi
	nvm use 10.16.0
	if [ $? -gt 0 ]; then
		>&2 echo "nvm could not switch to node version $node_version_bancor which is needed by bancor"
		exit 1	
	fi
fi


# start ganache subprocess 
# this is needed for bancor compile/deploy and also data boostrapping
ganache_bin=`which ganache-cli`
mkdir -p $d_lib/ganache.db
rm $d_lib/ganache.db/*
#$ganache_bin -i 42 -l 800000000 -g 2000000000 -s 666 -p 7545 -e 100000 --acctKeys $d_lib/ganache.accounts --db $d_lib/ganache.db 2> $d_log/ganache.log &
$ganache_bin -i 42 -l 800000000 -g 2000000000 -s 666 -p 7545 -e 100000 --acctKeys $d_lib/ganache.accounts --db $d_lib/ganache.db &
pid_ganache=$!
echo -n $pid_ganache > $d_run/ganache.pid
echo "waiting 3 secs for ganache to start (pid $pid_ganache)..."
sleep 3


# enter bancor dir and check for sanity
bancor_dir=$(realpath ${BANCOR_PATH:-$1})
echo "bancodir $bancor_dir"
if [ -z $bancor_dir ]; then
	bancor_dir=$d/contrib/bancor
fi
if [ ! -d $bancor_dir ]; then
	>&2 echo "bancor dir '$bancor_dir' not a dir"
	exit 1
fi
pushd $bancor_dir
bancor_commit=`git rev-parse HEAD`
if [ "$bancor_commit" != "$BANCOR_COMMIT" ]; then
	>&2 echo "wrong bancor version, need $BANCOR_COMMIT, have $bancor_commit"
	exit 1
fi
if [ ! -d "node_modules" ]; then # risky, doesn't check the contents
	# snyk never completes..
	mv package.json package.json.tmp
	jq '.scripts.prepare = "echo skipping prepare snyk because it just neeeever finishes :/"' package.json.tmp | jq '.snyk = false' > package.json
	rm package-lock.json
	npm install
	mv package.json.tmp package.json
fi
if [ ! -f ${bancor_dir}/node_modules/truffle/build/cli.bundled.js ]; then
	>&2 echo "cannot find truffle bin"
	exit 1
fi
truffle=${bancor_dir}/node_modules/truffle/build/cli.bundled.js
pushd solidity


# build and deploy bancor contracts
$truffle --network development migrate --reset
if [ "$?" -gt 0 ]; then
	>&2 echo "truffle migrate fail"
	exit 1
fi
popd
popd


# convenience variables for contracts
# needed by api calls for bootstrapping data
contract_names=()
contract_addresses=()
token_address=''
reserve_address=''
registry_address=''
i=0
for f in `ls $BANCOR_PATH/solidity/build/contracts/*.json`; do
	k=${f%%.*}
	k=${k##*/}
	j=`jq -r '.networks["42"].address' $f`
	if [ "$j" != 'null' ]; then
		address=`python ${d}/tools/ethereum_checksum_address.py ${j}`
		if [ "$address" == 'null' ]; then
			continue
		fi
		echo $k $address
		contract_names[i]=$k
		contract_addresses[i]=$address
		if [ "$k" == "SmartToken" ]; then
			token_address=$address
		fi
		if [ "$k" == "EtherToken" ]; then
			reserve_address=$address
		fi
		if [ "$k" == "ContractRegistry" ]; then
			registry_address=$address
		fi
		i=$((i+1))
	fi
done
echo "bancor contract names ${contract_names[@]}"
echo "bancor contract addresses ${contract_addresses[@]}"
converter_address=`python ${d}/tools/bancor_converter_registry.py --bancor-dir $BANCOR_PATH --registry-address $registry_address $token_address`


# determine master eth key
# TODO: no matter how fun sed is, this is a mess. Make an template for the generation script instead
master_private_key=`jq -r '.private_keys[]' ${d_lib}/ganache.accounts | head -n 1`
master_address=`python ${d}/tools/ethereum_checksum_address.py $(jq -r '.private_keys | to_entries[0].key' ${d_lib}/ganache.accounts)`
echo private key is $master_private_key
# todo move config to etc


# sanitize the sempo config files
echo sempodir $sempodir
pushd $sempodir/config_files
rm -vf secret/${DEPLOYMENT_NAME}_secret.ini
python generate_secrets.py -n ${DEPLOYMENT_NAME} 
# patch config templates
# thanks [200~https://stackoverflow.com/questions/16987648/update-var-in-ini-file-using-bash
sed -i '/^\[DATABASE\]$/,/^\[/ s/^port[[:space:]].*$/port = 5432/' secret/common_secrets.ini
sed -i '/^\[DATABASE\]$/,/^\[/ s/^password[[:space:]].*$/password = example/' secret/common_secrets.ini
# this is "url safe"
pepper="GMAntFF09UoSW7ZQBB5t6DS7SxUY+YxuJcuLksX9U1M="
#pepper=`dd if=/dev/urandom bs=1 count=32 | base64`
echo "pepper $pepper"
sed -i "/^\[APP\]$/,/^\[$/ s/^password_pepper[[:space:]].*$/password_pepper = $pepper/" secret/${DEPLOYMENT_NAME}_secrets.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^master_wallet_private_key[[:space:]].*$/master_wallet_private_key = 0x${master_private_key}/" secret/${DEPLOYMENT_NAME}_secrets.ini
pepper=`dd if=/dev/zero bs=1 count=32 | base64`
#sed -i "/^\[APP\]$/,/^\[$/ s/^password_pepper[[:space:]].*$/password_pepper = $pepper/" secret/test_secret.ini
sed -i "/^\[APP\]$/,/^\[$/ s/^database[[:space:]].*$/database = grassroots/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[APP\]$/,/^\[$/ s/^eth_database[[:space:]].*$/eth_database = grassroots_eth/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^http_provider[[:space:]].*$/http_provider = http:\/\/127.0.0.1:7545/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^websocket_provider[[:space:]].*$/websocket_provider =  http:\/\/127.0.0.1:7545/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[APP\]$/,/^\[$/ s/^eth_database[[:space:]].*$/eth_database = grassroots_eth/" public/test_config.ini
sed -i "/^\[APP\]$/,/^\[$/ s/^database[[:space:]].*$/database = grassroots/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^http_provider[[:space:]].*$/http_provider = http:\/\/127.0.0.1:7545/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^websocket_provider[[:space:]].*$/websocket_provider =  http:\/\/127.0.0.1:7545/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^contract_address[[:space:]].*$/contract_address = ${registry_address}/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^contract_address[[:space:]].*$/contract_address = ${registry_address}/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_address[[:space:]].*$/reserve_token_address = ${reserve_address}/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_address[[:space:]].*$/reserve_token_address = ${reserve_address}/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_name[[:space:]].*$/reserve_token_name = ${RESERVE_TOKEN_NAME}/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_name[[:space:]].*$/reserve_token_name = ${RESERVE_TOKEN_NAME}/" public/test_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_symbol[[:space:]].*$/reserve_token_symbol = ${RESERVE_TOKEN_SYMBOL}/" public/${DEPLOYMENT_NAME}_config.ini
sed -i "/^\[ETHEREUM\]$/,/^\[$/ s/^reserve_token_symbol[[:space:]].*$/reserve_token_symbol = ${RESERVE_TOKEN_SYMBOL}/" public/test_config.ini
# upgrade config file to ge fork requirements
grep -q '\[USSD\]' public/${DEPLOYMENT_NAME}_config.ini
if [ $? -gt 0 ]; then
	echo "missing USSD"
	cat <<EOF >> public/${DEPLOYMENT_NAME}_config.ini

[USSD]
valid_service_code = *483*46#
EOF
fi

grep -q '\[USSD\]' public/test_config.ini
if [ $? -gt 0 ]; then
	cat <<EOF >> public/test_config.ini

[USSD]
valid_service_code = *483*46#
EOF
fi

# wipe and re-install db
createdb -U postgres postgres
dropuser -U postgres -h 127.0.0.1 read_only
dropuser -U postgres -h 127.0.0.1 accenture
dropuser -U postgres -h 127.0.0.1 abundance
dropuser -U postgres -h 127.0.0.1 blockscience
dropdb -U postgres -h 127.0.0.1 grassroots
dropdb -U postgres -h 127.0.0.1 grassroots_eth
createdb -U postgres -h 127.0.0.1 postgres
createdb -U postgres -h 127.0.0.1 grassroots
createdb -U postgres -h 127.0.0.1 grassroots_eth
createuser -U postgres -h 127.0.0.1 read_only
# TODO: remove need for these users, should be "renderers" role only
createuser -U postgres -h 127.0.0.1 accenture
createuser -U postgres -h 127.0.0.1 abundance
createuser -U postgres -h 127.0.0.1 blockscience
if [ "$?" -gt "0" ]; then
	>&2 echo "db setup fail"
	exit 1
fi

# migrate db to latest version for both app and celery tasks
pushd $sempodir/app
python manage.py db upgrade
#alembic upgrade head
if [ "$?" -gt "0" ]; then
	>&2 echo "db migration fail"
	popd
	read foo
	exit 1
fi
popd
pushd $sempodir/eth_worker
alembic upgrade head


# start the celery task manager
# this is needed for seeding the bootstrap data
echo -e "\n>>> STARTING CELERY\n"
celery -A celery_tasks worker --loglevel=INFO --concurrency=1 --pool=eventlet -Q=low-priority,celery,high-priority --without-gossip --without-mingle &
pid_celery_work=$!
echo -n $pid_celery_work > $d_run/celery_task.pid
celery -A celery_tasks worker --loglevel=INFO --concurrency=1 --pool=eventlet -Q=processor --without-gossip --without-mingle &
pid_celery_proc=$!
echo -n $pid_celery_proc > $d_run/celery_proc.pid
popd
echo "waiting 5 secs for celery to start (pid $pid_celery_work, $pid_celery_proc)..."
sleep 5


# start the sempo app
# this is needed for seeding the bootstrap data
pushd $sempooverridedir
python ./run.py --single &
pid_app=$!
echo -n $pid_app > $d_run/sempo.pid
echo "waiting a bit for app to start..."
sleep 5


# seed script adds master organisation to db
# TODO: we should assemble a more easy-to-understand version of our own of data seeding and dev data generation
#pushd ${SEMPO_OVERRIDE_APP_PATH}/migrations
pushd ${SEMPO_PATH}/app/migrations
echo -e "\n>>> STARTING SEED SCRIPT\n"
tfatoken=`python3 seed.py ${APP_PROTO}://${APP_HOST}:${APP_PORT} ${LOCAL_EMAIL} ${LOCAL_PASSWORD} 0x0ec5dE3E0c2d5B5f7692C8ee92Df2ea8088941d3 | tail -n 1 | cut -b 3- | tr "'" " " | sed -e 's/ //'`
if [ "$?" -gt 0 ]; then
	>&2 echo seed script failed
	popd
	exit 1
fi
popd


# add master user
# hack db to set user as activated without having to invoke api
#curlflags='-v'
api_url="http://${APP_HOST}:${APP_PORT}/api/v1"
api_url_ge="http://${APP_HOST}:${APP_PORT}/api/v1/ge"
curl ${curlflags} -X POST ${api_url}/auth/register/ -d "{\"email\":\"${LOCAL_EMAIL}\",\"password\":\"${LOCAL_PASSWORD}\"}" -H 'Content-Type: application/json'
psql -U postgres -h 127.0.0.1 -d grassroots -c 'UPDATE public.user SET is_activated = true WHERE id = 1;'


# authenticate master user
d=`cat <<EOF
{
	"email": "$LOCAL_EMAIL",
	"password": "$LOCAL_PASSWORD"
}
EOF
`
echo $d | jq
r=`curl  -X POST ${api_url}/auth/request_api_token/ -d "$d" -H "Content-Type: application/json" | jq -r .auth_token`
auth_key=$r
echo "got auth key $auth_key"


# add reserve token
d=`cat <<EOF
{
	"name": "Grassroots XDAI Reserve Token",
	"symbol": "XDAIGR",
	"decimals": 18,
	"is_reserve": true,
	"address": "$reserve_address"
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X POST ${api_url}/token/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r


# add liquid token
d=`cat <<EOF
{
	"name": "Sarafu",
	"symbol": "SFU",
	"decimals": 18,
	"is_reserve": false,
	"address": "$token_address"
}
EOF
`
echo $d | jq
r=`curl  ${curlflags} -X POST ${api_url}/token/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
token_id=`echo $r | jq '.data.token.id'`
echo "got token id $token_id"


# add token converter
d=`cat <<EOF
{
	"reserve_address": "$reserve_address",
	"token_address": "$token_address",
	"exchange_address": "$converter_address",
	"registry_address": "$registry_address",
	"reserve_ratio_ppm": 250000
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X PUT ${api_url_ge}/exchange/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
exchange_id=`echo $r | jq '.data.exchange.id'`
echo "got exchange id $exchange_id"


# add liquid token organisation
d=`cat <<EOF
{
        "organisation_name": "Grassroots Economics",
        "custom_welcome_message_key": "grassroots",
        "timezone": "Africa/Nairobi",
        "country_code": "KE",
        "exchange_contract_id": $exchange_id,
	"token_id": 2
}
EOF
`
echo $d | jq
r=`curl ${curlflags} -X POST ${api_url}/organisation/ -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r
organisation_id=`echo $r | jq '.data.organisation.id'`


# add master user to liquid token organisation
# TODO: this step may not be necessary and may even cause trouble
d=`cat <<EOF
{
	"user_ids": [1],
	"is_admin": true
}
EOF
`
#echo $d | jq
#r=`curl ${curlflags} -X PUT ${api_url}/organisation/${organisation_id}/users -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
#echo $r

echo $d | jq
r=`curl ${curlflags} -X PUT ${api_url}/organisation/1/users -d "$d" -H "Authorization: ${auth_key}" -H "Accept: application/json" -H "Content-Type: application/json" --basic -u "$LOCAL_EMAIL:$LOCAL_PASSWORD"`
echo $r



# clean up sub-processes
echo -e "\n>>> KILL APP ($pid_app)\n"
kill -TERM $pid_app
rm -f $d_sum/sempo.pid

echo -e "\n>>> KILL CELERY ($pid_celery_work, $pid_celery_proc)\n"
kill -TERM $pid_celery_work
rm -f $d_run/celeryr_work.pid
kill -TERM $pid_celery_proc
rm -f $d_run/celery_proc.pid

sleep 5

echo -e "\n>>> KILL GANACHE ($pid_ganache)\n"
kill -TERM $pid_ganache
rm -f $d_run/ganache.pid

