#!/bin/bash

# TODO merge with path create script
d=`realpath $(dirname ${BASH_SOURCE[0]})`
d_log=$d/var/log/grassroots
d_lib=$d/var/lib/grassroots
d_run=$d/var/run/grassroots

for p in `find $d_run -iname '*.pid'`; do
	pp=`cat $p`
	echo "killing pid $pp"
	kill -TERM $pp
done

pushd $d

# is this necessary now?
. quick_env.sh
. .venv/bin/activate

echo -e "\n>>> STARTING GANACHE\n"
ganache_bin=`which ganache-cli`
$ganache_bin -v -i 42 -l 800000000 -g 2000000000 -s 666 -p ${WEB3_PORT} --acctKeys $d_lib/ganache.accounts --db $d_lib/ganache.db & #&> $d_log/ganache.log &
pid_ganache=$!
echo -n $pid_ganache > $d_run/ganache.pid
echo "waiting 3 secs for ganache to start (pid $pid_ganache)..."
sleep 3

echo -e "\n>>> STARTING CELERY \n"
celery -A celery_tasks worker --loglevel=INFO --concurrency=1 --pool=eventlet -Q=low-priority,celery,high-priority --without-gossip --without-mingle &
pid_celery_worker=$!
echo -n $pid_celery_worker > $d_run/celery_worker.pid
celery -A celery_tasks worker --loglevel=INFO --concurrency=1 --pool=eventlet -Q=processor --without-gossip --without-mingle &
pid_celery_beat=$!
echo -n $pid_celery_beat > $d_run/celery_beat.pid
echo "waiting 5 secs for ganache to start (pid $pid_celery)..."
sleep 5

pushd $SEMPO_OVERRIDE_APP_PATH
python3 run.py 
popd

echo -e "\n>>> KILL CELERY ($pid_celery)\n"
kill -TERM $pid_celery_beat
kill -TERM $pid_celery_worker
rm $d_run/celery_beat.pid
rm $d_run/celery_worker.pid

echo -e "\n>>> KILL GANACHE ($pid_ganache)\n"
kill -TERM $pid_ganache
rm $d_run/ganache.pid

popd
