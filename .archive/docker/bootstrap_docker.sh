#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# Description: Sets up virtualenv with the correct python interpreter and installs module dependencies for python and node
#
# adaptation of bootstrap script in repository root, for use in docker base container build
# the dependencies in Debians are quirky; libmysqlclient-dev (mysql_config) and npm (node) are in conflict.
# to work around it use nvm to install node

which node
if [ $? -ne 0 ]; then
	2>&1 echo "node missing" && exit 1
fi

which npm
if [ $? -ne 0 ]; then
	2>&1 echo "npm missing" && exit 1
fi

wd=$(realpath $(dirname ${BASH_SOURCE[0]}))
#confd=$wd/config_files # config_files now in repo, not necessary to create it
pushd $wd/..
. create_dirs.sh
sempodir=contrib/sempo
bancordir=contrib/bancor

# set up submodules
git submodule update --init
mkdir -p $sempodir/config_files/secret

# create the virtualenv
python3 -m venv .venv
if [ "$?" -gt 0 ]; then
	>&2 echo "venv fail"
	exit 1
fi

# remove the build dir
#rm -rf $tmpd

# install missing config-3.6m files that virtualenv doesn't put in
pyconfdv="config-${pyver}m"
mkdir -vp .venv/lib/python${pyver}/${pyconfdv}
install -vD $pyconfd/* .venv/lib/python${pyver}/${pyconfdv}/

# recursive install dependencies for python
# turn on virtualenv
source .venv/bin/activate
pip install wheel
pip install -r cli/requirements.txt
pip install -r src/sempo_extensions/requirements.txt

pushd $sempodir
pushd devtools
bash install_python_requirements.sh
popd
deactivate

# install dependencies for node
pushd app
npm install
npm run-script build
popd

# finish up
popd

