#!/bin/bash

d=$(realpath $(dirname ${BASH_SOURCE[0]}))
export DEPLOYMENT_NAME=gedocker
export LOCAL_EMAIL=ge-admin@localhost.localdomain
export LOCAL_PASSWORD=foorbar+++
export DATABASE_USER=postgres
export DATABASE_PASSWORD=example
export BANCOR_COMMIT=0453d904504ca713d98f94664d6f59dd34166e63
export BANCOR_PATH=$d/contrib/bancor
export SEMPO_PATH=$d/contrib/sempo
export SEMPO_OVERRIDE_PATH=$d/src
export SEMPO_OVERRIDE_APP_PATH=${SEMPO_OVERRIDE_PATH}/sempo_extensions/app
export APP_PROTO=http
export APP_HOST=127.0.0.1
export APP_PORT=9000

export PYTHONPATH=${SEMPO_OVERRIDE_PATH}:${SEMPO_PATH}:${SEMPO_PATH}/app:${SEMPO_PATH}/eth_worker/eth_manager:${SEMPO_PATH}/eth_worker:${SEMPO_PATH}/eth_worker/eth_manager/task_interfaces
