#!/bin/bash

# Author: Louis Holbrook <dev@holbrook.no> https://holbrook.no
# License: GPLv3
# Description: Build docker image suite for grassroots


tag='0.3'
flags='--no-cache'

if [ "$UID" -ne 0 ]; then
	>&2 echo WARNING: You are not root. Unless you have clever setup, you will have to be in order to run docker builds
fi

docker -l info build $flags --network app_backend -t grassrootseconomics/ubuntu:$tag -f Dockerfile.dev_ubuntu ..
docker -l info tag grassrootseconomics/ubuntu:$tag grassrootseconomics/ubuntu:latest

docker -l info build $flags --network app_backend -t grassrootseconomics/dev-eth-worker:$tag -f Dockerfile.dev_eth_worker ..
docker -l info tag grassrootseconomics/dev-eth-worker:$tag grassrootseconomics/dev-eth-worker:latest

docker -l info build $flags --network app_backend -t grassrootseconomics/dev-base:$tag -f Dockerfile.dev_base ..
docker -l info tag grassrootseconomics/dev-base:$tag grassrootseconomics/dev-base:latest

docker-compose -f docker-compose-build.yml -p app up db redis ganache app eth_worker eth_worker_processor &

docker -l info build $flags --network app_backend -t grassrootseconomics/dev-build:$tag -f Dockerfile.dev_build ..
docker -l info tag grassrootseconomics/dev-build:$tag grassrootseconomics/dev-build:latest

docker commit -m "Data generated $(date)" -a "$(whoami)" app_db_1 grassrootseconomics/db-dev:latest
docker commit -m "Data generated $(date)" -a "$(whoami)" app_ganache_1 grassrootseconomics/ganache-dev:latest

docker-compose -p app kill db redis ganache app eth_worker eth_worker_processor &

docker -l info build $flags --network app_backend -t grassrootseconomics/dev-run:$tag -f Dockerfile.dev_run ..
docker -l info tag grassrootseconomics/dev-run:$tag grassrootseconomics/dev-run:latest
