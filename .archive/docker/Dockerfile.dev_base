FROM grassrootseconomics/ubuntu:latest

ENV DEPLOYMENT_NAME gedocker
ENV LOCAL_EMAIL ge-admin@localhost.localdomain
ENV LOCAL_PASSWORD example
ENV DATABASE_HOST app_db_1.app_backend
ENV DATABASE_USER postgres
ENV DATABASE_PASSWORD example
ENV REDIS_HOST app_redis_1.app_backend
ENV REDIS_PORT 6379
ENV GANACHE_HOST app_ganache_1.app_backend
ENV GANACHE_PORT 7545
ENV GANACHE_PROTO http
ENV WEB3_HOST $GANACHE_HOST
ENV WEB3_PORT $GANACHE_PORT
ENV WEB3_PROTO $GANACHE_PROTO
ENV WEB3_URL ${GANACHE_PROTO}://${WEB3_HOST}:${WEB3_PORT}
ENV BANCOR_COMMIT 0453d904504ca713d98f94664d6f59dd34166e63
ENV BANCOR_PATH /opt/grassroots/contrib/bancor
ENV SEMPO_PATH /opt/grassroots/contrib/sempo
ENV SEMPO_OVERRIDE_PATH /opt/grassroots/src
ENV SEMPO_OVERRIDE_APP_PATH /opt/grassroots/src/sempo_extensions/app
ENV APP_HOST app_app_1.app_backend
ENV APP_PORT 9000
ENV APP_PROTO http
ENV PYTHONPATH /opt/grassroots/src:/opt/grassroots/contrib/sempo:/opt/grassroots/contrib/sempo/app:/opt/grassroots/contrib/sempo/eth_worker:/opt/grassroots/contrib/sempo/eth_worker/eth_manager:/opt/grassroots/contrib/sempo/eth_worker/eth_manager/task_interfaces

WORKDIR /opt

RUN git clone https://gitlab.com/nolash/grassroots-app grassroots

WORKDIR /opt/grassroots

RUN /bin/bash docker/bootstrap_docker.sh
RUN cp usr/share/bancor/truffle-config_docker_dev.js contrib/bancor/solidity/truffle-config.js
RUN cp usr/share/sempo/gedocker_config.ini contrib/sempo/config_files/public/
RUN cp usr/share/sempo/gedocker_secrets.ini contrib/sempo/config_files/secret/
RUN cp usr/share/sempo/common_secrets.ini contrib/sempo/config_files/secret/
RUN cp var/lib/grassroots/ganache.accounts var/lib/grassroots/

RUN cp ./src/sempo_extensions/app/run.py /opt/grassroots/src/sempo_extensions/app/
RUN cp ./src/sempo_extensions/app/server/api/hello_api.py /opt/grassroots/src/sempo_extensions/app/server/api/
RUN cp /src/sempo_extensions/app/server/api/eth_api.py /opt/grassroots/src/sempo_extensions/app/server/api/

ENTRYPOINT ["/bin/bash"]
