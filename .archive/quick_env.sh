#!/bin/bash

# TODO read these values from config template file instead
d=$(realpath $(dirname ${BASH_SOURCE[0]}))
export DEPLOYMENT_NAME=local
export LOCAL_EMAIL=example@example.com
export LOCAL_PASSWORD=foobar123+
export LOCAL_TFA_SECRET=
export DATABASE_HOST=localhost
export DATABASE_PORT=5432
export DATABASE_USER=postgres
export DATABASE_PASSWORD=example
export WEB3_HOST=localhost
export WEB3_PORT=7545
export REDIS_HOST=localhost
export REDIS_PORT=6379
export BANCOR_COMMIT=0453d904504ca713d98f94664d6f59dd34166e63
export BANCOR_PATH=$d/contrib/bancor
export SEMPO_PATH=$d/contrib/sempo
export SEMPO_OVERRIDE_PATH=$d/src
export SEMPO_OVERRIDE_APP_PATH=${SEMPO_OVERRIDE_PATH}/sempo_extensions/app
export APP_PROTO=http
export APP_HOST=127.0.0.1
export APP_PORT=9000
export APP_AUTH_TOKEN=
export APP_USSD_SERVICE_CODE=*483*46#

export PYTHONPATH=${SEMPO_OVERRIDE_PATH}:${SEMPO_PATH}:${SEMPO_PATH}/app:${SEMPO_PATH}/eth_worker/eth_manager:${SEMPO_PATH}/eth_worker:${SEMPO_PATH}/eth_worker/eth_manager/task_interfaces
