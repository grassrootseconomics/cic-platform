#!/usr/bin/env bash

git submodule update --init
# Patch returns exit code 1 if patch already applied
./patches/patch.sh || true

./patches/override.sh `pwd`/src/sempo_extensions `pwd`/contrib/sempo
./patches/override.sh `pwd`/src/ussd_extensions `pwd`/contrib/ussd

# Generate secrets
[ ! -d ./src/common/config_files/secret ] && mkdir ./src/common/config_files/secret
pushd ./src/common/config_files
python3 generate_secrets.py -n docker_test
popd

# Build client
pushd ./contrib/sempo/app
npm install
npm run-script build
popd
