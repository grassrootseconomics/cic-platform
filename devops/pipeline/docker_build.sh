#!/usr/bin/env bash

docker image pull ${REGISTRY}/server:latest || true
docker image pull ${REGISTRY}/ussd:latest || true
docker image pull ${REGISTRY}/eth_worker:latest || true

docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 app
docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 ussd
docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 eth_worker
