#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash --keep REGISTRY --keep PIPELINE_MASTER_WALLET_PRIVATE_KEY --keep DOCKER_HOST --keep DOCKER_BUILDKIT --keep COMPOSE_DOCKER_CLI_BUILD --keep GIT_SSL_CAINFO --keep DOCKER_DRIVER --keep AWS_ACCESS_KEY_ID --keep AWS_SECRET_ACCESS_KEY --keep AWS_REGION --keep CIC_FILES_LOCALE_ENGINE --keep CIC_FILES_LOCALE_REMOTE_LOCATION --keep CIC_FILES_LOCALE_REMOTE_PATH --keep CIC_FILES_LOCALE_LOCAL_PATH --keep CIC_FILES_LOCALE_ENTRIES ../../pipeline.nix

echo GIT_SSL_CAINFO $GIT_SSL_CAINFO

set -o errexit -o nounset -o pipefail

docker info

FILE=.envrc
if [ -f "$FILE" ]; then
    echo "Sourcing $FILE"
    source .envrc
else 
    echo "$FILE does not exist."
fi

export CONTAINER_MODE=TEST
export PIPELINE_MASTER_WALLET_PRIVATE_KEY=$PIPELINE_MASTER_WALLET_PRIVATE_KEY

[ -d './tmp/pipeline' ] && rm -Rf ./tmp/pipeline
mkdir -p ./tmp/pipeline
git clone . ./tmp/pipeline

pushd ./tmp/pipeline
./devops/pipeline/pre_build.sh
./devops/pipeline/docker_build.sh
./devops/pipeline/test_backend.sh
./devops/pipeline/test_frontend.sh
popd

[ -d './tmp/pipeline' ] && rm -Rf ./tmp/pipeline
