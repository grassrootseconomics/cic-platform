#!/usr/bin/env bash

docker-compose down -v
docker-compose up --scale ussd=0 --scale pgbouncer=0 --exit-code-from app
