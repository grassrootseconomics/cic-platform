#!/bin/sh
set -o errexit -o nounset -o pipefail

cat <<EOF | kubectl apply -n $NAMESPACE -f -
apiVersion: v1
kind: Secret
metadata:
  name: app-secrets
type: Opaque
stringData:
  DATABASE_PASSWORD: $POSTGRESQL_PASSWORD
  DATABASE_HOST: $DATABASE_HOST
  DATABASE_USER: $DATABASE_USER
  DATABASE_NAME: $DATABASE_NAME
---
EOF