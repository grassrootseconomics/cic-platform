#!/bin/sh
set -o errexit -o nounset -o pipefail

cat <<EOF | kubectl apply -f -
apiVersion: v1
data:
  postgresql-password: $PGADMIN_PASSWORD
  postgresql-replication-password: $PGREPLICA_PASSWORD
kind: Secret
metadata:
  name: infra-staging-postgresql-credentials
  namespace: infra-staging
type: Opaque
---
EOF