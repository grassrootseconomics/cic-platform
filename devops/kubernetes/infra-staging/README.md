# Troubleshooting

If you have trouble logging in to postgres after update the admin password could have rolled. It's recoverable on the postgres disk. 
https://github.com/helm/charts/issues/16251

Use helmReleases/generate-admin-password.sh to create admin secrets before deploying the chart.

The secret format is:

```
apiVersion: v1
data:
  postgresql-password: $PGADMIN_PASSWORD
  postgresql-replication-password: $PGREPL_PASSWORD
kind: Secret
metadata:
  name: infra-staging-postgresql-credentials
  namespace: infra-staging
type: Opaque

```

export POSTGRESQL_PASSWORD=$(kubectl get secret --namespace infra-staging infra-staging-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

Gotta add the repo for upgrades smh

helm repo add bitnami https://charts.bitnami.com/bitnami

helm upgrade my-release stable/postgresql \
    --set postgresqlPassword=[POSTGRESQL_PASSWORD] \
    --set replication.password=[REPLICATION_PASSWORD]

