#!/bin/sh
set -o errexit -o nounset -o pipefail

cat <<EOF | kubectl apply -n $NAMESPACE -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ganache
  labels:
    app: ganache
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ganache

  # Pod template
  template:
    metadata:
      labels:
        app: ganache
    spec:
      containers:
        - name: ganache
          image: trufflesuite/ganache-cli:v6.9.1
          resources:
            limits:
              cpu: "0.5"
              memory: 512Mi
            requests:
              cpu: "0.25"
              memory: 256Mi
          args: ["--db=/ganache", "-l", "$ETH_GASLIMIT", "-i", "$ETH_NETWORK_ID", "-e", "$ETH_DEV_INITIAL_BALANCE", "-d",  "true",  "-m", "'$ETH_MNEMONIC'"]
          imagePullPolicy: Always
          ports:
            - containerPort: 8545
          volumeMounts:
            - mountPath: /ganache
              name: ganache
      volumes:
        - name: ganache
          persistentVolumeClaim:
            claimName: ganache
---
apiVersion: v1
kind: Service
metadata:
  name: ganache
spec:
  ports:
    - protocol: TCP
      port: 8545
      targetPort: 8545
  selector:
    app: ganache
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ganache
spec:
  storageClassName: "do-block-storage"
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
---
EOF
