#!/bin/sh
set -o errexit -o nounset -o pipefail

cat <<EOF | kubectl apply -n $NAMESPACE -f -
apiVersion: v1
kind: Secret
metadata:
  name: postgres
type: Opaque
stringData:
  password: $PGPASSWORD
  replicaPassword: $PGPASSWORD_REPLICA
---
EOF