with import <nixpkgs> {};

# To use a binary cache, install Cachix:
# nix-env -ibA cachix -f https://cachix.org/api/v1/install
# cachix use sarafu-dev
# To update the binary cache:
# nix-build default.nix -A env | cachix push sarafu-dev

let
  default = import ./default.nix {};
  inherit (default) env pkgs python pythonPackages;
  script-install = pkgs.writeShellScriptBin "install-deps" ''
    python -m venv venv
    # virtualenv venv
    source venv/bin/activate
    python -m pip install  --no-cache-dir --extra-index-url https://pip.grassrootseconomics.net:8433 -r requirements.txt
    pushd $SEMPO_PATH
    python -m pip install --no-cache-dir -r test_requirements.txt
    popd
    pushd $SEMPO_PATH/app
    python -m pip install --no-cache-dir -r requirements.txt
    popd
    pushd $SEMPO_PATH/eth_worker
    python -m pip install --no-cache-dir -r requirements.txt
    popd
    pushd $SEMPO_PATH/worker
    python -m pip install --no-cache-dir -r requirements.txt
    popd
    pushd $SEMPO_PATH/app
    npm install
    npm run-script build
    popd
  '';
  script-setup = pkgs.writeShellScriptBin "setup" ''
    source venv/bin/activate
    bash devtools/quick_setup_script.sh
  '';
  script-start = pkgs.writeShellScriptBin "start" ''
    pushd $SEMPO_PATH/app
    npm run-script build
    popd
    source venv/bin/activate
    python -u $SEMPO_PATH/app/run.py
  '';
  script-tests = pkgs.writeShellScriptBin "tests" ''
    # prerequisite:
    # - patching and overwrite
    # - test secrets are generated
    # - npm i + npm run build is run in sempo app
    # - python init.py locale is run
    source venv/bin/activate
    pushd $SEMPO_PATH/app
    DEPLOYMENT_NAME=test pytest -x -v --log-cli-level DEBUG test_app
    popd
  '';
  script-dev = pkgs.writeShellScriptBin "dev" ''
    pushd $SEMPO_PATH/app
    npm run dev &
    popd
    source venv/bin/activate
    python -u $SEMPO_PATH/app/run.py
  '';
  script-pipeline = pkgs.writeShellScriptBin "pipeline" ''
    ./devops/pipeline/start.sh
  '';
  script-reset-submodules = pkgs.writeShellScriptBin "reset-submodules" ''
    git submodule foreach --recursive git reset --hard
    git submodule foreach --recursive git clean -x -f -d
  '';
  script-update-submodules = pkgs.writeShellScriptBin "update-submodules" ''
    git submodule update --init
  '';
in
stdenv.mkDerivation rec {
  name = "ge-shell";
  buildInputs = [ env ] ++ [
    script-install
    script-setup
    script-start
    script-dev
    script-pipeline
    script-tests
    script-reset-submodules
    script-update-submodules 
  ];
  PYTHONPATH = python.pkgs.makePythonPath [
    python.pkgs.pandas
    python.pkgs.numpy
  ];
  GIT_SSL_CAINFO = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
  LD_LIBRARY_PATH = "${stdenv.cc.cc.lib}/lib/libstdc++.so.6";
  shellHook = "
    source .envrc
    unset SOURCE_DATE_EPOCH

    # In order of search priority
    export PYTHONPATH=$SEMPO_PATH:${PYTHONPATH}:`pwd`/venv/${python.sitePackages}/

    echo 'To install development environment Python dependencies, run: install-deps'
    echo 'To setup & seed development environment, run: setup'
    echo 'To start app after setup, run: start'
    echo 'To start app with hot reloading after setup, run: dev'
    echo 'App host: localhost:9000'
    echo 'Username and password: admin@acme.org / C0rrectH0rse'
  ";
}
