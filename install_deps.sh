#!/bin/bash

# this is a replacement script for nix that can be used to install deps
# if nix for some reason doesn't work

cic_index_host='pip.grassrootseconomics.net:8433'
cic_index_url_flags="--extra-index-url https://$cic_index_host"
nocache=''
if [ ! -z $CIC_PIP_NO_CACHE ]; then
	nocache='--no-cache-dir'
fi
set -e
python -m venv venv
# virtualenv venv
source venv/bin/activate
python -m pip install $cic_index_url_flags $nocache -r requirements.txt
pushd $SEMPO_PATH
python -m pip install $cic_index_url_flags $nocache -r test_requirements.txt
popd
pushd $SEMPO_PATH/app
python -m pip install $cic_index_url_flags $nocache -r requirements.txt
popd
pushd $SEMPO_PATH/eth_worker
python -m pip install $cic_index_url_flags $nocache -r requirements.txt
popd
pushd $SEMPO_PATH/worker
python -m pip install $cic_index_url_flags $nocache -r requirements.txt
popd
deactivate
set +e

echo ">>> REMEMBER"
echo "* use node 10.16.0"
echo "* customize and source your .envrc"
echo "* run patches/override.sh [...] and patches/patch.sh"
echo "* activate your venv"
