@node Contents
@chapter Contents

@node Sempo application
@section Sempo application
The Grassroots Economics platform is an adaptation of the @strong{Sempo Blockchain} application.


@subsection Application server
Provides API and interactions with the @emph{'main'} database.

The Grassroots Economics platform only makes partial use of the API endpoints available in the Sempo platform, specifically those that provide user and account management and transaction overviews, along with the @abbr{USSD} endpoint.


@subsection Celery workers
Signs transactions, and monitors and interacts with an @code{evm} blockchain. Uses a separate @emph{'eth_worker'} database.


@subsection React client
Dashboard type administration intreface providing access to transactions, users and accounts.


@node Customizations
@section Customizations
A number of customizations have been made to the original Sempo application code to extend functionality needed by Grassroots Economics.

To ease with upstream compatibility, customizations do not modify existing data models. As far as possible, customizations also do not modify existing upstream code.

API extensions are published with the @file{/api/v2/} prefix. Source code resides in @file{src/sempo_extensions/app/server/api2}.


@anchor{locationdata}
@subsection Location data
An OSM-based hierarchical geolocation data model, with API endpoints to add and modify these entries to the database, and associate them with user accounts. 

A new @code{location} table has been added to the SQLAlchemy models. A @code{user_extensions} table has been added to enable user association with location data.

@subsection Database-only account management API
API endpoints have been added to enable account creation without triggering Celery worker blockchain events.

This is required in order to enable external contract deployment.

@subsection SQLAlchemy models for User and Organisation
In order to enable a model object to represent the "user extension" used for the @xref{locationdata,Location Data}, a modification had to me made in User and Organisation model code to allow for polymorphism.

@subsection Self-service USSD wallets
Enabled account creation from USSD. This code is hosted in the @ref{externalresources_ussd,sempo 'ussd fork'}.

@subsection SMS notifications log
All outgoing SMS messages are logged to database. Adds a new database table.

@subsection AWS SES email support
Enable sending of email (for user account invitations and password resets) with AWS SES.


@node Supporting components
@section Supporting components

 
@subsection Customizations for token conversions
The Sempo platform does not have working support for exchange between tokens on the Bancor network. Furthermore, the existing code is written for a contract suite version from august 2019, and interfaces have changed fundamentally since then.

A new suite of celery workers are required for this purpose. These are being developed in a separate repository: @url{https://gitlab.com/grassrootseconomics/cic-indexer}. The original Sempo celery workers will be re-used, as far as it is possible.


@subsection Keystore and EIP-155 signer
There is no convenient way to produce EIP-155 signatures with the current code. The target blockchain, @strong{Bloxberg}, needs EIP-155 signatures.

Since a new component thus needs to be written, the opportunity presents itself to prepare for improved security oof key storage, by moving the signing functionality to a separate component.

Short-term, this is a component constributed by a team member, which provides signatures using web3 json-rpc calls, making the component entirely pluggable. Since the python web3 library enables middleware filters, enabling multiplexing of signatures and blockchain transaction/query calls within the same web3 instance. The json-rpc interface is served on a UNIX-socket, and must be running in the same instance(s) as the client code.

The component is hosted here: @url{https://gitlab.com/nolash/crypto-dev-signer}.



@node External repositories
@section External repositories
External resources in are found in the @code{contrib} directory. The subdirectories each point to a separate @code{git} sub-repository with revisions currently used by the platform.

@anchor{externalresources_sempo}
@subsection sempo
A snapshot of the Sempo Blockchain repository.

@anchor{externalresources_sempo_patching}
@subsubsection Patching
Adaptations to the repository are applied using the common @command{diff} and @command{patch} Unix tools. The source files to build the patches are in @file{src/sempo_patch}.

To build patches execute the following steps:

@enumerate
@item
Reset and clean @file{contrib/sempo}.
@item
Remove existing patches: @file{rm patches/*.patch}.
@item
Execute @file{bash patches/diff.sh}.
@end enumerate

To apply patches execute the following steps:

@enumerate
@item
Reset and clean @file{contrib/sempo}.
@item
Execute @command{bash patches/patch.sh}.
@end enumerate

@subsubsection Extensions
The Grassroots Economics instance extends the Sempo applications with additional api endpoints and database objects. The source files for the extensions are in @file{src/sempo_extensions}.

To apply the extensions execute @command{bash patches/override.sh src/sempo_extensions contrib/sempo}.

The command will fail if any of the files to be copied already exist in the target.


@anchor{externalresources_ussd}
@subsection ussd
A fork of the Sempo Blockchain repository, containing a number of changes made to the USSD functionality, which is intertwined with the general application code.

Due to the complexity and coupling of these changes, they have not been pulled out as patches as described above in the @ref{externalresources_sempo, sempo section}.

@subsubsection Patching

Follow the same procedure as in @ref{externalresources_sempo_patching,,Patching} above, exchanging @kbd{sempo} in path names with @kbd{ussd}.


@subsection bancor
The official Bancor protocol solidity contract repository, with revision currently supported by the platform.

Bancor provides the contracts together with the @strong{Truffle} framework. The files in @file{src/bancor} provide specific configuration and migration scripts used to deploy the contracts. The standard Truffle upgradability / migrations contracts have been removed.

@subsubsection Deployment
The migration steps consist of two scripts:

@enumerate
@item
Necessary contracts for the network itself, along with a single reserve token.
@item
Two convertible tokens @emph{Bert Token} and @emph{Ernie Token}, the latter with half the reserve and supply as the former.
@end enumerate

Ensure that the same seed is used for deployments as used for seeding the application database.


@node Blockchain network
@section Blockchain network
Grassroots Economics uses the XDAI POA network. Currently a remove RPC node is used, a local node does not exist. (This is, of course, far from ideal).

A move to Bloxberg POA network is imminent. Before this move redundant nodes must be set up to enable low-latency access to @emph{openethereum} nodes to query and interact with the network.

Bloxberg runs on id 8995 and enforces EIP-155 signatures.
