@node Setup
@chapter Setup

@section Requirements
@itemize
@item
@code{python >= 3.6}
@item
@code{nodejs == 10.16.0} (ganache does not work with higher versions)
@end itemize


@subsection Python dependencies
Defined across the following files:

@itemize
@item
@file{./requirements.txt}
@item
@file{./docker/pgbouncer/requirements.txt}
@item
@file{./contrib/sempo/app/requirements.txt}
@item
@file{./contrib/sempo/test_requirements.txt}
@item
@file{./contrib/sempo/worker/requirements.txt}
@item
@file{./contrib/sempo/eth_worker/requirements.txt}
@end itemize


@section Environment
Setting up the correct environment involves:

@itemize @bullet
@item
setting correct @strong{environment variables}.
@item
installing correct @strong{python} dependencies
@item
building the @strong{Sempo React app}.
@end itemize


@subsection Using nix
A @strong{nix} setup is included for environment predictability.

For now, refer to the main repository @emph{README} file for more information.


@anchor{localsetup}
@section Local setup
A local setup means that all daemons needed are run as individual host processes. The steps are:

@enumerate
@item
Execute @command{bash ./installdeps.sh}, which will:
@itemize @minus 
@item
install and activate @code{virtualenv}
@item
@command{pip install} all requirements.
@item
Build the @emph{React app}.
@end itemize
@item
Make sure nodejs interpreter version is @code{10.16.0}. The @command{nvm} tool enables changing active interpreter versions.
@item
Make a copy of @file{.envrc_example}, modify as needed and @command{source} it.
@item
Activate @command{virtualenv}.
@item
Clean and reset submodules.
@item
Execute @command{bash devtools/local_setup_script.sh}
@end enumerate

The effects of these steps will be:

@itemize @bullet
@item
Database schemas are created.
@item
Patches and extensions are applied. @xref{sempo,,,overview}.
@item
A 'common secrets' file is generated in @file{contrib/sempo/config_files/secret}.
@item
Contracts are deployed and two default tokens created. @xref{bancor,,,overview}.
@item
And organisation and login is created in the Sempo app for the reserve token. The default username and password is @kbd{admin@@acme.org} / @kbd{C0rrectH0rse}.
@item
The two default tokens are added to the Sempo app, and organisations and logins are created for them.
@item
Private keys for tokens and deployments are added to the wallets managed by @emph{Sempo celery workers}.
@item
Access to the two default token organisations is granted to the admin user of the reserve token.
@item
Registry and token addresses saved in @file{tokens.json}. Private keys from seed stored in @file{keys.json}.
@end itemize

@subsection Typical issues

@table @samp
@item ganache network mismatch
@itemize
@item
Check that an existing ganache-cli is not running
@item
Remove existing ganacheDB folder in @file{contrib/sempo}
@end itemize
@item ganache/truffle socket timeout
@itemize
@item
Check that an existing ganache-cli is not running
@end itemize

@end table


@section Remote setup
Currently it is not possible to seed a new remote instance through the pipeline. Remote seeding must therefore be executed from the local host.

The steps are the same as for @ref{localsetup, Local Setup}, except:

@itemize @bullet
@item
Make sure relevant environment variables match those of the deployment.
@item
Port-forward connections to @emph{Postgres}, @emph{Redis} and @emph{Ethereum network provider}.
@item
Execute @command{bash devtools/remote_setup_script.sh} in place of @command{bash devtools/local_setup_script.sh}.
@item

@end itemize


@section Running the pipeline
For now, refer to the main repository @emph{README} file for more information.
