@node Overview
@chapter Overview
This technical platform is an intermediate solution for the requirements of the Grassroots Economics project.

The document describes the current state of the components and setup.

Details and information about strategy, goals and further development will be published elsewhere.


@section Development goals
The Sempo application is a @acronym{SaaS} that uses its local database state as the source of truth, and 'copies' currency transfers to an Ethereum blockchain.

Grassroots Economics must provide a combination of hosted keys aswell as independent wallets where the transfers do not originate from the Sempo application itself.

The main development goal is to transition to a system state where the application consumes blockchain events in the same manner as any other independent client, and where the database is @emph{not} the source of truth.


@section Technologies

@itemize @bullet
@item
@strong{Python Flask/Sqlalchemy}: API endpoints, database model interaction. 
@item
@strong{Python alembic}: Database migrations
@item
@strong{Python Celery}: Local (signing) and remote (transactions, query) Ethereum tasks.
@item
@strong{React}: Web client application.
@item
@strong{PostgreSQL}: Database backend.
@item
@strong{Redis}: Broker and results for Celery workers.
@end itemize


