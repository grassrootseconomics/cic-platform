#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p kubectl git --keep NAMESPACE --keep CLUSTER --keep KUBE_URL --keep KUBE_CA_PEM_FILE --keep KUBE_TOKEN
set -o errexit -o nounset -o pipefail

TAG=$(git rev-parse --short HEAD)

# kubectl config set-cluster $CLUSTER --server="$KUBE_URL" --certificate-authority="$KUBE_CA_PEM_FILE"
# kubectl config set-credentials admin --token="$KUBE_TOKEN"
# kubectl config set-context default --cluster="$CLUSTER" --namespace="$NAMESPACE" --user=admin
# kubectl config use-context default
kubectl config get-contexts
kubectl cluster-info

kubectl set image deployment/eth-worker-high-priority eth-worker=registry.gitlab.com/grassrootseconomics/cic-platform/eth_worker:$TAG
kubectl set image deployment/eth-worker-low-priority eth-worker=registry.gitlab.com/grassrootseconomics/cic-platform/eth_worker:$TAG
kubectl set image deployment/eth-worker-processor eth-worker=registry.gitlab.com/grassrootseconomics/cic-platform/eth_worker:$TAG

if [ "$NAMESPACE" = "sarafu-staging" ]
then
    echo "WARNING: Skipping beat-worker deployment"
    # kubectl set image deployment/beat-worker eth-worker=registry.gitlab.com/grassrootseconomics/cic-platform/eth_worker:$TAG
fi

kubectl set image deployment/app app=registry.gitlab.com/grassrootseconomics/cic-platform/server:$TAG
kubectl set image deployment/ussd ussd=registry.gitlab.com/grassrootseconomics/cic-platform/ussd:$TAG

