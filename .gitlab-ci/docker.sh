#!/usr/bin/env nix-shell
#!nix-shell -i bash -p docker_compose docker git nodejs-13_x
set -o errexit -o nounset -o pipefail

build_image() {
  GIT_HASH=$1

  ./devops/pipeline/pre_build.sh

  docker image pull ${REGISTRY}/server:latest || true
  docker image pull ${REGISTRY}/ussd:latest || true
  docker image pull ${REGISTRY}/eth_worker:latest || true
  docker image pull ${REGISTRY}/pgbouncer:latest || true

  docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 app
  docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 ussd
  docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 eth_worker
  docker-compose build --build-arg BUILDKIT_INLINE_CACHE=1 pgbouncer
}

push_image() {
  TAG=$1

  docker tag server:latest ${REGISTRY}/server:${TAG}
  docker push ${REGISTRY}/server:${TAG}
  docker tag server:latest ${REGISTRY}/server:latest
  docker push ${REGISTRY}/server:latest

  docker tag ussd:latest ${REGISTRY}/ussd:${TAG}
  docker push ${REGISTRY}/ussd:${TAG}
  docker tag ussd:latest ${REGISTRY}/ussd:latest
  docker push ${REGISTRY}/ussd:latest
  
  docker tag eth_worker:latest ${REGISTRY}/eth_worker:${TAG}
  docker push ${REGISTRY}/eth_worker:${TAG}
  docker tag eth_worker:latest ${REGISTRY}/eth_worker:latest
  docker push ${REGISTRY}/eth_worker:latest

  docker tag pgbouncer:latest ${REGISTRY}/pgbouncer:${TAG}
  docker push ${REGISTRY}/pgbouncer:${TAG}
  docker tag pgbouncer:latest ${REGISTRY}/pgbouncer:latest
  docker push ${REGISTRY}/pgbouncer:latest
}

# GIT_HASH=$(git rev-parse --short HEAD)
GIT_HASH=$TAG
BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)

echo "Building git hash $GIT_HASH and branch name $BRANCH_NAME"

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# if [ $BRANCH_NAME == "master" ]
# then
#   build_image $GIT_HASH
#   push_image $GIT_HASH
# else
#   build_image $GIT_HASH
# fi

build_image $GIT_HASH
push_image $GIT_HASH
