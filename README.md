# Grassroots CIC platform

This repository contains customizations for the Sempo platform required by the Grassroots Economics project. It is provided for reference for anyone seeking to make similar customizations. It's provided without any warranty or responsibility of use.

Some of the following commands use Nix. This is optional, but is recommended and will make your life easier.

## Submodules

Initialize the git submodules - these will be initialized in the `contrib/` directory.

```bash
git submodule update --init
```

If you're running the pipeline and tests, and want to reset any uncommitted changes in the submodules, run:

```bash
git -C contrib/sempo reset --hard
git -C contrib/sempo clean -f
```

## Patches

This is a work in progress.

To make changes to Sempo submodule files, make a copy of the relevant file in the same subdirectory under `src/sempo`, then create and apply the patch:

```bash
# This will create a `sempo.patch` file
./patches/diff.sh
# This will apply the `sempo.patch` file to the submodule. Do not commit these changes.
./patches/patch.sh
```

For files that need to be removed or replaced entirely in the Sempo submodule, we'll need to give this some more thought - either overriding the PYTHONPATH, or removing the files in a script.

## Extensions

The `override.sh` script will recursively create directories, and copy files, into the submodule. If a file already exists, it will fail - these are extensions that should not conflict with existing source code.

Usage: `override.sh $FROM $TO` with full path name

```bash
./patches/override.sh `pwd`/src/sempo_extensions `pwd`/contrib/sempo
./patches/override.sh `pwd`/src/ussd_extensions `pwd`/contrib/ussd
```

## Environment variables

Create a copy of the `.envrc_example` file. Then fill in the necessary secrets and source the file in your environment:

```bash
cp .envrc_example .envrc
source .envrc
```

To make this easier for yourself, check out `direnv`.

## Nix
> Nix is a powerful package manager for Linux and other Unix systems that makes package management reliable and reproducible. Share your development and build environments across different machines. 

It creates a robust sandboxed environment for software development, but can also just be used for managing system dependencies.

See https://nixos.org/download.html

Installing Nix (required) and Cachix (optional, but recommended for faster builds):

```bash
curl -L https://nixos.org/nix/install | sh
nix-env -ibA cachix -f https://cachix.org/api/v1/install
# If you'd like to use a build cache (recommended!)
cachix use sarafu-dev
# Replace sarafu-dev with ge-dev-darwin if you're using macOS
```

Entering the Nix development environment will make all necessary system dependencies available - this may take a while if you're not using Cachix and if it's your first time entering the shell:

```bash
nix-shell
```
**or**

```bash
# Using the --pure flag ensures no system dependencies or environment variables are used, this can be useful, but isn't required.
nix-shell --pure --keep NIX_PATH
```

After entering the shell environment, a number of commands will be available to you, such as: `install-deps`, `setup`, `start`. Further instructions will be printed to console.

Within the Nix `shell.nix` you'll see the commands that are run at each step, these could be run manually.

If you'd like to make these dependencies available permanently outside the shell, run: `nix-env -if default.nix`

# Installation

The app requires Redis and Postgres. Please make sure they are running when performing any of the steps in this document.

```bash
install-deps
```

# Development

To setup the necessary system components, and seed the database, use the following command which will give you an interactive CLI:

```bash
setup
```

In another shell, after the previous `setup` command has completed, but is still running:

```bash
start
```

**or** for hot-reloading of the React app:

```bash
dev
```

## Source files

* `src/app`: Application source files (added to PYTHONPATH)
* `src/sempo`: Patches for Sempo submodule (patched over existing submodule source files)

# CI/CD Pipeline

## GitLab

See `.gitlab-ci.yml` and `.gitlab-ci/` for configuration of pipeline stages. See `devops/pipeline/` for pipeline scripts.

NB: the pipeline, for now, will only run for parent repo branches, not forks. This is for security I guess, and also because it doesn't seem to be possible with GitLab: https://gitlab.com/gitlab-org/gitlab/-/issues/11934

The pipeline runs for PR branches, and for the master branch. The GitLab container registry is used for Docker images. Branch `master` is deployed to staging automatically, whereas production is deployed manually for now, but in future from the `production` branch.

## Local

As much as possible, this will recreate what is run in the GitLab CI/CD job - running tests and building Docker images. GitLab uses `.gitlab-ci.yml` for orchestrating the pipeline, whereas locally we use the files in the `vagrant` directory.

To run the pipeline locally:

```bash
nix-shell
pipeline
```

Please note, this will clone the current repo into a temporary directory `tmp/pipeline` - any uncommitted files won't be included in the pipeline job.

# Dependencies

## System

If you'd rather install your own system dependencies, for whatever system you may have (Linux, macOS, etc.), you can find the dependencies listed in the `default.nix` and `shell.nix` files under `paths` and `buildInputs` respectively.

## Submodules

See [Setup](#-setup) section for initializing git submodules.

* python 3.7.3 (chosen to match bancor dep version)
* ganache-cli 6.7.0 (testing)
* bancor contracts; https://github.com/nolash/contracts (as submodule)
  - same as https://github.com/bancorprotocol/contracts tag v0.5.16, adding convenience bootstrapping scripts and contract migration for ge relevant contracts

Note that deployed sempo bancor contracts were branched on aug 17th 2019, which means that the production app is using 0.4.8 or 0.4.9.

# Contract deployment

For development locally, first set `MNEMONIC`, `RPC_HOST` & `RPC_PORT` env variables, then:

```bash
install-deps
./devtools/start_ganache.sh &
./devtools/contract_deployment.sh
./devtools/get_ganache_keypair.sh
```

For deployment to staging, ensure `MNEMONIC` matches, then:

```bash
kubectl port-forward -n sarafu-staging $GANACHE_POD_NAME $RPC_PORT:8545
./devtools/contract_deployment.sh
./devtools/get_ganache_keypair.sh
```

# Kubernetes

* `devops/kubernetes/` - root directory
* `devops/kubernetes/deployments` - deployments for system containers, like the Python application and eth_worker

## Ganache deployment

The following instructions are for deploying a Ganache container, with a persistent volume for the Ganache database directory, to a test or staging environment. It allows you to set both the BIP39 12 word `MNEMONIC` and `NAMESPACE` (e.g. sarafu-staging) for the deployment.

```bash
export MNEMONIC=''
export NAMESPACE='sarafu-staging'
./devops/kubernetes/deployments/ganache.sh
```

## Deploying to staging Ganache

* Config file deployment name: `seed`

```bash
# See `contract_deployment.sh` and .envrc_example for other environment variables that need to be set
export POD_NAME=ganache-_
export NAMESPACE=sarafu-staging
kubectl port-forward $POD_NAME -n $NAMESPACE $RPC_PORT:8545
./devtools/contract_deployment.sh
# A logfile will be created with all the contract addresses listed
```

This script will use Ganache to get the first key pair from the mnemonic set as an environment variable. See `.envrc_example`.

```bash
./devtools/get_ganache_keypair.sh 
# Ganache account keys generated at ganache_keys.secret
# _ADDRESS_
# _PRIVATE_KEY_
```

```bash
./devtools/remote_seed.sh
```

## Updating environment variables via ConfigMaps

Kubernetes passes environment variables to the deployed containers using a `ConfigMap`. They are quite self explanatory to use - the environment variables are set in the `ConfigMap`. Any secrets should rather be deployed using a Kubernetes secret.

* `devops/kubernetes/deployments/configmaps/`

```bash
# Choose the ConfigMap, pass to argument `-f`, and select relevant namespace
kubectl apply -f staging/app.yaml --namespace sarafu-staging
# To check the newly deployed/updated ConfigMap
kubectl describe configmaps --namespace sarafu-staging app
```

## Useful one-liners

View logs for all pods in deployment, rather than per pod. To get all logs, remove the `--follow` flag and give the `--tail` flag a try.

```bash
kubectl logs -l app=app --container=app -n sarafu-staging --follow
kubectl logs -l app=ussd --container=ussd -n sarafu-staging --follow
```

Update the Docker container image for a deployment. This is used for doing a deployment to staging or production, after the image has been built. If Kubernetes can't find the specified image, it won't be deployed - so this is safe. Assuming the deployment has a rolling-update deployment strategy, there will also be minimal downtime.

```bash
kubectl set image deployment/app app=registry.gitlab.com/grassrootseconomics/cic-platform/server:$TAG
```

## License

All code in this repository is licensed under GPLv3.

Submodules are licensed independently.
